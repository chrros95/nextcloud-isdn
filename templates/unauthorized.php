<style>
thead tr th{
    font-weight: bold;
}
.left_space {
    padding-left: 2em;
}
</style>
<div class="section" data-date="2021-133">
	<h2>Internet Services der DLRG Nottuln (ISDN)</h2>
    <div>Du besitzt leider keine Rechte für einen der Services. Wenn du meinst, dass das falsch ist, wende dich bitte an einen Administrator.</div>
    <h2>Andere DLRG Services</h2>
    <table>
        <thead>
            <tr>
                <th scope="col">Service</th>
                <th class="left_space" scope="col">Link</th>
            </tr>
        </thead>
        <tbody> <?php
        foreach ($_["links"] as $link) {
            print_unescaped('<tr><th scope="row">'.$link->getLabel().'</th><td class="left_space"><a target="'.$link->getTarget().'" href="'.$link->getHref().'">'.$link->getHref().'</a></td></tr>');
        }?>
        </tbody>
    </table>
</div>
