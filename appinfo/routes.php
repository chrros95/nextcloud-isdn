<?php

$routes = [
    'routes' => [
        [
            'name' => 'page#checkIn',
            'url' => '/checkin',
            'verb' => 'GET',
        ],
        [
            'name' => 'page#index',
            'url' => '/{path}',
            'verb' => 'GET',
            'requirements' => ['path' => '(?!api\/).+'],
            'defaults' => ["path"=>null]
        ]
    ],
    'resources' => array(),
];

$subjects = array(
    ['permission', 'Permissions'],
    ['role', 'Roles'],
    ['person', 'Persons'],
    ['contact_details', 'ContactDetails'],
    ['event', 'Events'],
    ['check_in', 'CheckIns'],
    ['link', 'Links'],
    ['mail', 'Mails'],
    ['scheduled_event', 'ScheduledEvents'],
);

foreach ($subjects as $subject) {
    $routes['routes'][] = [
        'name' => $subject[0].'#restore',
        'url' => '/api/{apiVersion}/'.$subject[1].'/{id}',
        'requirements' => ['apiVersion' => 'v1'],
        'verb' => 'PATCH'
    ];
    $routes['resources'][$subject[0]] = [
        'url' => '/api/{apiVersion}/'.$subject[1],
        'requirements' => ['apiVersion' => 'v1']
    ];
}
return $routes;
