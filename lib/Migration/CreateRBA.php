<?php
namespace OCA\ISDN\Migration;

use OCP\Migration\IOutput;
use OCP\Migration\IRepairStep;
use OCP\Constants;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Service\PermissionService;
use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Db\Permission;
use OCA\ISDN\Db\Role;

class CreateRBA implements IRepairStep
{

    /** @var PermissionService */
    private $permissionService;
    /** @var RoleService */
    private $roleService;

    public function __construct(
        PermissionService $permissionService,
        RoleService $roleService
    ) {
        $this->permissionService = $permissionService;
        $this->roleService = $roleService;
    }

    /**
     * @return string
     * @since 9.1.0
     */
    public function getName()
    {
        return 'ISDN: Create RBA';
    }

    /**
     * @param IOutput $output
     * @throws \Exception in case of failure
     */
    public function run(IOutput $output) : void
    {
        $this->createPermissions($output);
        $this->createRoles($output);
    }

    /**
     * @param IOutput $output
     */
    protected function createPermissions(IOutput $output) : void
    {
        $permissions = array(
            [1, "Roles"],
            [2, "Persons"],
            [3, "ContactDetails"],
            [4, "Events"],
            [5, "CheckIns"],
            [6, "Links"],
            [7, "Mails"],
            [8, "ScheduledEvents"]
        );

        foreach ($permissions as $pArray) {
            $permission = new Permission();
            $permission->setId($pArray[0]);
            $permission->setName($pArray[1]);
            $this->permissionService->save($permission);
        }
    }

    /**
     * @param IOutput $output
     */
    protected function createRoles(IOutput $output) : void
    {
        try {
            $role = $this->roleService->findById(1);
        } catch (DoesNotExistException $e) {
            $role = new Role("Event Manager");
            $role->setId(1);
        }
        $role->addPermission(2, Constants::PERMISSION_ALL);
        $role->addPermission(3, Constants::PERMISSION_ALL);
        $role->addPermission(4, Constants::PERMISSION_ALL);
        $role->addPermission(5, Constants::PERMISSION_ALL);
        $role->addPermission(8, Constants::PERMISSION_ALL);
        $this->roleService->save($role);

        try {
            $role = $this->roleService->findById(2);
        } catch (DoesNotExistException $e) {
            $role = new Role("Check-In Assistant");
            $role->setId(2);
        }
        $role->addPermission(
            2,
            Constants::PERMISSION_READ | Constants::PERMISSION_UPDATE | Constants::PERMISSION_CREATE
        );
        $role->addPermission(
            3,
            Constants::PERMISSION_READ | Constants::PERMISSION_UPDATE
            | Constants::PERMISSION_CREATE | Constants::PERMISSION_DELETE
        );
        $role->addPermission(
            4,
            Constants::PERMISSION_READ
        );
        $role->addPermission(
            5,
            Constants::PERMISSION_READ | Constants::PERMISSION_CREATE
        );
        $role->addPermission(
            8,
            Constants::PERMISSION_READ
        );
        $this->roleService->save($role);

        try {
            $role = $this->roleService->findById(3);
        } catch (DoesNotExistException $e) {
            $role = new Role("All Users");
            $role->setId(3);
        }
        $role->addPermission(6, Constants::PERMISSION_READ);
        $role->addPermission(7, Constants::PERMISSION_CREATE);
        $role->addOwner(Role::GROUP_USERS, "group");
        $this->roleService->save($role);

        try {
            $role = $this->roleService->findById(4);
        } catch (DoesNotExistException $e) {
            $role = new Role("Anonymous");
            $role->setId(4);
        }
        $role->addOwner(Role::GROUP_ANONYMOUS, "group");
        $role->addPermission(5, Constants::PERMISSION_CREATE);
        $this->roleService->save($role);
    }
}
