<?php
namespace OCA\ISDN\Migration;

use Doctrine\DBAL\Schema\Table;

use OCP\Migration\SimpleMigrationStep;
use OCP\DB\ISchemaWrapper;

class ESimpleMigrationStep extends SimpleMigrationStep
{
    /**
     * @param array<mixed> $ridProps
     * @param array<mixed> $valueProps
     */
    protected function createRelationTable(
        ISchemaWrapper $schema,
        string $tableName,
        string $ridType = "integer",
        $ridProps = null,
        string $valueType = "string",
        $valueProps = null,
        bool $disableValueIndex = false
    ) :ISchemaWrapper {
        if (is_null($ridProps)) {
            $ridProps = [
                'notnull' => true,
            ];
        }
        if (is_null($valueProps)) {
            $valueProps = [
                'notnull' => false,
                'length' => 200
            ];
        }
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table->addColumn('id', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('rid', $ridType, $ridProps);
            $table->addColumn('value', $valueType, $valueProps);
            $table->setPrimaryKey(['id', 'rid']);
            if (!$disableValueIndex) {
                $table->addIndex(['value'], $tableName.'_idx');#)
            }
        }
        return $schema;
    }

    protected function assignDefaultColumns(Table $table) : Table
    {
        $table->addColumn('id', 'integer', [
        'autoincrement' => true,
        'notnull' => true,
        ]);
        $table->addColumn('deleted_at', 'integer', [
        'notnull' => false
        ]);
        return $table;
    }
}
