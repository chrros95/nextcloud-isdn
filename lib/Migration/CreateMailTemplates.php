<?php
namespace OCA\ISDN\Migration;

use OCP\Migration\IOutput;
use OCP\Migration\IRepairStep;
use OCP\Constants;
use OCP\Files\IAppData;
use OCP\Files\NotFoundException;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\Service\MailTemplateService;
use OCA\ISDN\Db\MailTemplate;

class CreateMailTemplates implements IRepairStep
{

    /** @var MailTemplateService */
    private $mailTemplateService;
    /** @var IAppData */
    private $appData;

    public function __construct(
        IAppData $appData,
        MailTemplateService $mailTemplateService
    ) {
        $this->mailTemplateService = $mailTemplateService;
        $this->appData = $appData;
    }

    /**
     * @return string
     * @since 9.1.0
     */
    public function getName()
    {
        return 'ISDN: Create default templates';
    }

    /**
     * @param IOutput $output
     * @throws \Exception in case of failure
     */
    public function run(IOutput $output) : void
    {
        $this->createTemplates($output);
        $this->createTemplateDir($output);
    }

    /**
     * @param IOutput $output
     */
    protected function createTemplates(IOutput $output) : void
    {
        $templates = array(
            ["person_code", "Check-In-Code Template", "Dein persönlicher Chek-In-Code"],
        );

        foreach ($templates as $templateArray) {
            try {
                $this->mailTemplateService->find(false, $templateArray[0]);
            } catch (DoesNotExistException $e) {
                $template = new MailTemplate($templateArray[0], $templateArray[1], $templateArray[2]);
                $this->mailTemplateService->save($template);
            }
        }
    }

    /**
     * @param IOutput $output
     */
    protected function createTemplateDir(IOutput $output) : void
    {
        try {
            $this->appData->getFolder("mail_templates");
        } catch (NotFoundException $e) {
            $this->appData->newFolder("mail_templates");
        }
    }
}
