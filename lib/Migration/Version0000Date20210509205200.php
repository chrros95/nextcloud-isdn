<?php
namespace OCA\ISDN\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\IOutput;

use OCA\ISDN\AppInfo\Application;

class Version0000Date20210509205200 extends ESimpleMigrationStep
{

  /**
  * @param IOutput $output
  * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
  * @param array<mixed> $options
  * @return null|ISchemaWrapper
  */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options)
    {
      /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();

        $schema = $this->createPermissionTable($schema);
        $schema = $this->createRoleTable($schema);
        $schema = $this->createCheckInTable($schema);
        //$schema = $this->createContactTable($schema);
        $schema = $this->createPersonTable($schema);
        $schema = $this->createEventTable($schema);
        $schema = $this->createScheduledEventTable($schema);
        $schema = $this->createLinkTable($schema);
        $schema = $this->createMailTable($schema);
        $schema = $this->createMailTemplateTable($schema);

        return $schema;
    }

    private function createPermissionTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_permission';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('name', 'string', [
            'notnull' => true,
            'length' => 200,
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['name'], $tableName.'_idx');
        }
        return $schema;
    }

    private function createRoleTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_role';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                "length" => 200
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['name'], $tableName.'_idx');#
        }
        $this->createRelationTable($schema, $tableName."_u", "string", [
            'notnull' => false,
            'length' => 200
        ]);
        $this->createRelationTable($schema, $tableName."_g", "string", [
            'notnull' => false,
            'length' => 200
        ]);
        $this->createRelationTable(
            $schema,
            $tableName."_p",
            "integer",
            ["notnull" => true],
            "integer",
            ["notnull" => true]
        );
        return $schema;
    }

    private function createCheckInTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_checkin';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('person', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('check_in', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('event', 'integer', [
                'notnull' => true
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['person','check_in'], $tableName.'_idx');#
            $table->addIndex(['event', 'person'], $tableName.'_idx_evt');#
        }
        return $schema;
    }

    private function createContactTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_contact';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('type', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('data', 'string', [
                'notnull' => true,
                'length' => 4000
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['data'], $tableName.'_idx');#
        }
        return $schema;
    }

    private function createPersonTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_person';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('forename', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('surename', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('birthday', 'integer', [
                'notnull' => false
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['forename', "surename"], $tableName.'_idx');

            $this->createRelationTable($schema, $tableName."_c", "string", [
                'notnull' => false,
                'length' => 200
            ]);
            $this->createRelationTable($schema, $tableName."_r", "string", [
                'notnull' => false,
                'length' => 200
            ], "string", [
                'notnull' => false,
                'length' => 200
            ]);
        }
        return $schema;
    }

    private function createEventTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_event';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('start', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('end', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('address', 'text', [
                'notnull' => false
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['name', "start", "end"], $tableName.'_idx');
        }
        return $schema;
    }

    private function createScheduledEventTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_scheduledevent';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('duration', 'string', [
                'notnull' => true,
                'length' => 10
            ]);
            $table->addColumn('address', 'text', [
                'notnull' => false
            ]);
            $table->addColumn('schedule', 'text', [
                'notnull' => true
            ]);
            $table->addColumn('last_generated', 'integer', [
                'notnull' => false
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['name'], $tableName.'_idx');

            $this->createRelationTable($schema, $tableName."_e");
        }
        return $schema;
    }

    private function createLinkTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_link';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('label', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('target', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('href', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['label'], $tableName.'_idx');
        }
        return $schema;
    }

    private function createMailTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_mail';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('template', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('to', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('vars', 'text', [
                'notnull' => true
            ]);
            $table->addColumn('send_at', 'integer', [
                'notnull' => false
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['to', "send_at"], $tableName.'_idx');
            $table->addIndex(["send_at"], $tableName.'_idx_send');
        }
        return $schema;
    }

    private function createMailTemplateTable(ISchemaWrapper $schema):ISchemaWrapper
    {
        $tableName = Application::ID.'_mailtemplate';
        if (!$schema->hasTable($tableName)) {
            $table = $schema->createTable($tableName);
            $table = $this->assignDefaultColumns($table);
            $table->addColumn('label', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('subject', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('from', 'string', [
                'notnull' => false,
                'length' => 200
            ]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(["label"], $tableName.'_idx');
            $table->addIndex(["name"], $tableName.'_idx_name');
            $table->addIndex(["subject"], $tableName.'_idx_subject');
        }
        return $schema;
    }
}
