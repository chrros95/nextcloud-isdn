<?php
namespace OCA\ISDN\Migration;

use OCP\Migration\IOutput;
use OCP\Migration\IRepairStep;
use OCP\Constants;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\Service\LinkService;
use OCA\ISDN\Db\Link;

class CreateLinks implements IRepairStep
{

    /** @var LinkService */
    private $linkService;

    public function __construct(
        LinkService $linkService
    ) {
        $this->linkService = $linkService;
    }

    /**
     * @return string
     * @since 9.1.0
     */
    public function getName()
    {
        return 'ISDN: Create default Links';
    }

    /**
     * @param IOutput $output
     * @throws \Exception in case of failure
     */
    public function run(IOutput $output) : void
    {
        $this->createLinks($output);
    }

    /**
     * @param IOutput $output
     */
    protected function createLinks(IOutput $output) : void
    {
        $links = array(
            [1, "Internet Service Center (ISC)", "https://dlrg.net"],
            [2, "Webmail", "https://webmail.dlrg.de"],
            [3, "Typo3 (CMS)", "https://tv.dlrg.de"]
        );

        foreach ($links as $linkArray) {
            try {
                $link = $this->linkService->findById($linkArray[0]);
            } catch (DoesNotExistException $e) {
                $link = new Link();
                $link->setId($linkArray[0]);
            }
            $link->setLabel($linkArray[1]);
            $link->setTarget("_blank");
            $link->setHref($linkArray[2]);
            $this->linkService->save($link);
        }
    }
}
