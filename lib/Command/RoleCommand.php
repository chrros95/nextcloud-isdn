<?php
namespace OCA\ISDN\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Helper\Table;

use OC\Core\Command\Base;
use OCP\IUserManager;
use OCP\IGroupManager;
use OCP\ILogger;

use OCA\ISDN\Db\Role;
use OCA\ISDN\Service\RoleService;

class RoleCommand extends Base
{

    /** @var IUserManager */
    protected $userManager;
    /** @var IGroupManager */
    protected $groupManager;
    /** @var ILogger */
    private $logger;
    /** @var RoleService */
    private $roleService;

    public function __construct(
        IUserManager $userManager,
        IGroupManager $groupManager,
        ILogger $logger,
        RoleService $roleService
    ) {
        parent::__construct();
        $this->userManager = $userManager;
        $this->groupManager = $groupManager;
        $this->logger = $logger;
        $this->roleService = $roleService;
    }

    protected function configure(): void
    {
        $this->setName('isdn:role')
            ->setDescription('Manage Roles')
            ->addOption('with-owners', 'w', InputOption::VALUE_NONE, 'list role owners')
            ->addOption(
                'users',
                'u',
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'list of users to grant a role'
            )
            ->addOption(
                'groups',
                'g',
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'list of groups to grant a role'
            )
            ->addArgument('action', InputArgument::REQUIRED, 'Choose betwen list, add, remove')
            ->addArgument(
                "role",
                InputArgument::OPTIONAL,
                'role used for the action'
            );

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$output instanceof ConsoleOutput) {
            throw new \LogicException('This command accepts only an instance of "ConsoleConsoleOutput".');
        }
        switch ($input->getArgument("action")) {
            case "list":
                return $this->listRoles($input, $output);
            case "add":
                return $this->addRoleOwner($input, $output);
            case "remove":
                return $this->removeRoleOwner($input, $output);
            default:
                $output->writeln("<error>The action is unknown.</error>");
                return 2;
        }
    }

    protected function listRoles(InputInterface $input, ConsoleOutput $output) : int
    {
        $table = $this->getTable($output);
        $table->render();
        if (!is_null($input->getArgument("role"))) {
            $roles = $input->getArgument("role");
            if (!is_array($roles)) {
                $roles = [$roles];
            }
            foreach ($roles as $role) {
                $role = $this->getRole($role);
                $table->appendRow($this->getRoleAsRow($role));
                $this->abortIfInterrupted();
            }
            return 0;
        }

        $limit = 20;
        $offset = 0;
        do {
            $roles = $this->roleService->findAll(true, $limit, $offset);
            foreach ($roles as $role) {
                $table->appendRow($this->getRoleAsRow($role));
                $this->abortIfInterrupted();
            }
            $offset += $limit;
        } while (count($roles) === $limit);
        return 0;
    }

    protected function addRoleOwner(InputInterface $input, ConsoleOutput $output) : int
    {
        if (is_null($input->getOption("users")) && is_null($input->getOption("groups"))) {
            $output->writeln("<error>At least one user or group is needed.</error>");
            return 1;
        }
        $users = $input->getOption("users");
        $users = is_array($users) ?  $users : [$users];
        $groups = $input->getOption("groups");
        $groups = is_array($groups) ?  $groups : [$groups];
        $roles = $input->getArgument("role");
        if (is_null($roles)) {
            $output->writeln("<error>At least one role must be given.</error>");
            return 1;
        }
        if (!is_array($roles)) {
            $roles = [$roles];
        }
        foreach ($roles as $role) {
            $role = $this->getRole($role);
            foreach ($users as $user) {
                if (is_string($user) && $this->userManager->userExists($user)) {
                    $role->addOwner($user, "user");
                } else {
                    $output->writeln("<comment>User '".$user."' doesn't exists.</comment>");
                }
            }
            foreach ($groups as $group) {
                if (is_string($group) && $this->groupManager->groupExists($group)) {
                    $role->addOwner($group, "group");
                } else {
                    $output->writeln("<info>Group '".$group."' doesn't exists.</info>");
                }
            }
            $this->roleService->save($role);
            $this->printRole($input, $output, $role);
        }
        return 0;
    }

    protected function removeRoleOwner(InputInterface $input, ConsoleOutput $output) : int
    {

        if ($input->getOption("users") && $input->getOption("groups")) {
            $output->writeln("<error>At least one user or group is needed.</error>");
            return 1;
        }

        $users = $input->getOption("users");
        $users = is_array($users) ?  $users : [$users];
        $groups = $input->getOption("groups");
        $groups = is_array($groups) ?  $groups : [$groups];
        $roles = $input->getArgument("role");
        if (is_null($roles)) {
            $output->writeln("<error>At least one role must be given.</error>");
            return 1;
        }
        if (!is_array($roles)) {
            $roles = [$roles];
        }
        foreach ($roles as $role) {
            $role = $this->getRole($role);
            foreach ($users as $user) {
                if (is_string($user)) {
                    $role->removeOwner($user, "user");
                }
            }
            foreach ($groups as $group) {
                if (is_string($group)) {
                    $role->removeOwner($group, "groups");
                }
            }
            $this->roleService->save($role);
            $this->printRole($input, $output, $role);
        }
        return 0;
    }

    protected function printRole(InputInterface $input, ConsoleOutput $output, Role $role) : void
    {
        $table = $this->getTable($output);
        $table->addRow($this->getRoleAsRow($role));
        $table->render();
    }

    /**
     * @return array<string>
     */
    protected function getRoleAsRow(Role $role) : array
    {
        $permissions = "";
        foreach ($role->getPermissions() as $id => $value) {
            if ($value instanceof \OCA\ISDN\Db\Permission) {
                $permissions .= $value->getName()."(".$value->getLevel().")".", ";
            }
        }
        $users = "";
        foreach ($role->getUsers() as $id => $value) {
            $users .= $id.", ";
        }
        $groups = "";
        foreach ($role->getGroups() as $id => $value) {
            $groups .= $id.", ";
        }
        return [
            "".$role->getId(),
            $role->getName(),
            trim(trim($permissions), ","),
            trim(trim($users), ","),
            trim(trim($groups), ",")
        ];
    }

    private function getRole(string $idOrName) : Role
    {
        if (is_numeric($idOrName)) {
            return $this->roleService->findById(intval($idOrName), true);
        } else {
            return $this->roleService->find(true, $idOrName);
        }
    }

    private function getTable(ConsoleOutput $output) : Table
    {
        $section = $output->section();
        $table = new Table($section);
        $table->setHeaders(["ID", "Name", "Permissions", "Users", "Groups"]);
        return $table;
    }
}
