<?php
namespace OCA\ISDN\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Helper\Table;

use OC\Core\Command\Base;
use OCP\ILogger;

use OCA\ISDN\Service\SEWOBEService;

class PersonCommand extends Base
{

    /** @var ILogger */
    private $logger;
    /** @var SEWOBEService */
    private $sewobeService;

    public function __construct(
        ILogger $logger,
        SEWOBEService $sewobeService
    ) {
        parent::__construct();
        $this->logger = $logger;
        $this->sewobeService = $sewobeService;
    }

    protected function configure(): void
    {
        $this->setName('isdn:person')
            ->setDescription('Manage Persons')
            ->addArgument('action', InputArgument::REQUIRED, 'sync');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$output instanceof ConsoleOutput) {
            throw new \LogicException('This command accepts only an instance of "ConsoleConsoleOutput".');
        }
        switch ($input->getArgument("action")) {
            case "sync":
                return $this->syncPersons($input, $output);
            default:
                $output->writeln("<error>The action is unknown.</error>");
                return 2;
        }
    }

    protected function syncPersons(InputInterface $input, ConsoleOutput $output) : int
    {
        $persons = $this->sewobeService->sync();
        $section = $output->section();
        $table = new Table($section);
        $table->setHeaders(["ID", "Forename", "Surename", "Refrences"]);
        foreach ($persons as $k => $p) {
            $table->addRow([$p->getId(), $p->getForename(), $p->getSurename(), implode(",", $p->getReferences())]);
        }
        $table->render();
        return 0;
    }
}
