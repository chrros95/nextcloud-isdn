<?php
namespace OCA\ISDN\BackgroundJob;

use OCA\ISDN\Service\SEWOBEService;

class SyncSEWOBE extends \OC\BackgroundJob\TimedJob
{
    /** @var SEWOBEService */
    private $sewobeService;

    public function __construct(
        SEWOBEService $sewobeService
    ) {
        $this->setInterval(60*60*24);
        $this->sewobeService = $sewobeService;
    }

    /**
     * @param  mixed $argument
     * @throws \Exception
     */
    protected function run($argument): void
    {
        $this->sewobeService->sync();
    }
}
