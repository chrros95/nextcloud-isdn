<?php
namespace OCA\ISDN\BackgroundJob;

use OCA\ISDN\Service\MailService;
use OCA\ISDN\Service\MailTemplateService;

class SendUnsendMails extends \OC\BackgroundJob\TimedJob
{
    /** @var MailService */
    private $mailService;
    /** @var MailTemplateService */
    protected $mailTemplateService;

    public function __construct(
        MailService $mailService,
        MailTemplateService $mailTemplateService
    ) {
        $this->setInterval(1);
        $this->mailService = $mailService;
        $this->mailTemplateService = $mailTemplateService;
    }

    /**
     * @param  mixed $argument
     * @throws \Exception
     */
    protected function run($argument): void
    {
        $mails = $this->mailService->findUnsendMails();
        foreach ($mails as $mail) {
            $this->mailService->sendMail($mail);
        }
    }
}
