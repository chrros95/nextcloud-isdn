<?php
namespace OCA\ISDN\BackgroundJob;

use OCP\ILogger;
use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Service\LinkService;
use OCA\ISDN\Service\MailService;
use OCA\ISDN\Service\MailTemplateService;
use OCA\ISDN\Service\PermissionService;
use OCA\ISDN\Service\PersonService;
use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\ScheduledEventService;
use OCA\ISDN\Service\EventService;

class DeleteObjects extends \OC\BackgroundJob\TimedJob
{
    /** @var ILogger */
    private $logger;
    /** @var LinkService */
    private $linkService;
    /** @var MailService */
    private $mailService;
    /** @var MailTemplateService */
    private $mailTemplateService;
    /** @var PermissionService */
    private $permissionService;
    /** @var PersonService */
    private $personService;
    /** @var RoleService */
    private $roleService;
    /** @var ScheduledEventService */
    private $scheduledEventService;
    /** @var EventService */
    private $eventService;

    public function __construct(
        ILogger $logger,
        LinkService $linkService,
        MailService $mailService,
        MailTemplateService $mailTemplateService,
        PermissionService $permissionService,
        PersonService $personService,
        RoleService $roleService,
        ScheduledEventService $scheduledEventService,
        EventService $eventService
    ) {
        $this->setInterval(1);
        $this->logger = $logger;
        $this->linkService = $linkService;
        $this->mailService = $mailService;
        $this->mailTemplateService = $mailTemplateService;
        $this->permissionService = $permissionService;
        $this->personService = $personService;
        $this->roleService = $roleService;
        $this->scheduledEventService = $scheduledEventService;
        $this->eventService = $eventService;
    }

    /**
     * @param  mixed $argument
     * @throws \Exception
     */
    protected function run($argument): void
    {
        $services = [ 'link', 'mail', 'mailTemplate', 'permission', 'person', 'role', 'scheduledEvent', 'event'];
        foreach ($services as $name) {
            $service = $name."Service";
            $deletedObjects = $this->$service->findAllDeleted();
            foreach ($deletedObjects as $object) {
                $t = $object->getDeletedAt();
                if ($t && (time() - $t->getTimestamp()) >  60) {
                    try {
                        $this->$service->delete($object);
                        $this->logger->info('Deleted '.$name.'-entity '.$object->getId());
                    } catch (\Exception $e) {
                        $this->logger->error('Failed to delete object: '.$e->getMessage(), ['exception' => $e]);
                        $this->logger->logException($e, ['app' => Application::ID]);
                    }
                }
            }
        }
    }
}
