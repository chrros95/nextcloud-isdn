<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\Mail\IMailer;
use OCP\Mail\IEMailTemplate;
use OCP\Mail\IMessage;
use OCP\Files\IAppData;
use OCP\Files\SimpleFS\ISimpleFolder;
use OCP\App\IAppManager;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\MailTemplate;
use OCA\ISDN\Db\MailTemplateMapper;
use OCA\ISDN\Db\Mail;
use OCA\ISDN\Mail\MailerTemplate;

/**
 * @extends AService<MailTemplate,MailTemplateMapper>
 */
class MailTemplateService extends AService
{

    /** @var IAppManager */
    protected $appManager;
    /** @var IAppData */
    protected $appData;

    public function __construct(
        ILogger $logger,
        MailTemplateMapper $mapper,
        IAppManager $appManager,
        IAppData $appData
    ) {
        parent::__construct($logger, $mapper);
        $this->appManager = $appManager;
        $this->appData = $appData;
    }

    public function createMailerTemplateById(int $id):MailerTemplate
    {
        $template = $this->findById($id);
        return $this->createMailerTemplate($template);
    }

    public function createMailerTemplate(MailTemplate $template):MailerTemplate
    {
        $mailTemplateFolder = $this->appData->getFolder("mail_templates");
        $mailerTemplate = new MailerTemplate();
        if ($template->getFrom()) {
            $mailerTemplate->setFrom(explode(",", $template->getFrom()));
        }
        $mailerTemplate->setSubject($template->getSubject());
        $mailerTemplate->setHTMl($this->getAppDataFile($mailTemplateFolder, $template->getLabel().".html"));
        $mailerTemplate->setText($this->getAppDataFile($mailTemplateFolder, $template->getLabel().".txt"));
        return $mailerTemplate;
    }

    private function getAppDataFile(ISimpleFolder $folder, string $filename):string
    {
        if ($folder->fileExists($filename)) {
            $file = $folder->getFile($filename);
            return $file->getContent();
        }
        $appPath = $this->appManager->getAppPath(Application::ID);
        $path = $appPath."/templates/mail_templates/".$filename;
        if (file_exists($path)) {
            $content = file_get_contents($path);
            if ($content !== false) {
                return $content;
            }
        }
        return "";
    }
}
