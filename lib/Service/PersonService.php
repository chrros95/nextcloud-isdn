<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\Person;
use OCA\ISDN\Db\PersonMapper;

/**
 * @extends AService<Person,PersonMapper>
 */
class PersonService extends AService
{

    public function __construct(
        ILogger $logger,
        PersonMapper $mapper
    ) {
        parent::__construct($logger, $mapper);
    }

    public function findByReference(string $type, string $id) : Person
    {
        return $this->mapper->findByReference($type, $id);
    }
}
