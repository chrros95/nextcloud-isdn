<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\Db\EEntity;
use OCA\ISDN\Db\EQBMapper;

/**
 * @template         T of EEntity
 * @template         M of EQBMapper<T>
 */
class AService
{

    /** @var ILogger */
    protected $logger;
    /** @var M */
    protected $mapper;

    /**
     * @param ILogger $logger
     * @param M $mapper
     */
    public function __construct(
        ILogger $logger,
        EQBMapper $mapper
    ) {
        $this->logger = $logger;
        $this->mapper = $mapper;
    }

    /**
     * @param T $entity
     * @return T
     */
    public function enrich(EEntity $entity):EEntity
    {
        return $entity;
    }

    /**
     * @param int $id
     * @return T
     */
    public function findById(int $id, bool $enrich = false) : EEntity
    {
        $entity = $this->mapper->findById($id);
        if ($enrich) {
            $entity = $this->enrich($entity);
        }
        return $entity;
    }

    /**
     * @return array<T>
     */
    public function findAllDeleted() : array
    {
        return $this->mapper->findAllDeleted();
    }

    /**
     * @param bool $enrich
     * @param int|null $limit
     * @param int|null $offset
     * @param array<array<string>> $orderBy
     * @return array<T>
     */
    public function findAll(
        bool $enrich = false,
        ?int $limit = 20,
        ?int $offset = null,
        ?array $orderBy = null
    ):array {
        $entities = $this->mapper->findAll($limit, $offset, $orderBy);
        if ($enrich) {
            foreach ($entities as $entity) {
                $entity = $this->enrich($entity);
            }
        }
        return $entities;
    }

    /**
     * @param bool $enrich
     * @param mixed $searchParams
     * @return T
     */
    public function find(bool $enrich = false, ...$searchParams):EEntity
    {
        $entity = $this->mapper->find(...$searchParams);
        if ($enrich) {
            $entity = $this->enrich($entity);
        }
        return $entity;
    }

    /**
     * @param T $entity
     * @return T
     */
    protected function update(EEntity $entity) : EEntity
    {
        return $this->mapper->update($entity);
    }

    /**
     * @param T $entity
     * @return T
     */
    protected function create(EEntity $entity) : EEntity
    {
        return $this->mapper->insert($entity);
    }

    /**
     * @param T $entity
     * @return T
     */
    public function save(EEntity $entity) : EEntity
    {
        if ($entity->getId() && $this->mapper->exists($entity->getId())) {
            return $this->update($entity);
        }
        return $this->create($entity);
    }

    /**
     * @param T $entity
     * @return T
     */
    public function delete(EEntity $entity) : EEntity
    {
        return $this->mapper->delete($entity);
    }

    /**
     * @param integer $id
     * @return T
     */
    public function markAsDeleted(int $id) : EEntity
    {
        $entity = $this->findById($id, false);
        $entity->setDeletedAt(new \DateTime());
        return $this->save($entity);
    }

    /**
     * @param integer $id
     * @return T
     */
    public function removeDeletionMark(int $id) : EEntity
    {
        $entity = $this->findById($id, false);
        $entity->setDeletedAt(null);
        return $this->save($entity);
    }
}
