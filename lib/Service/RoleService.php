<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\IUser;
use OCP\IGroupManager;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\Role;
use OCA\ISDN\Db\EEntity;
use OCA\ISDN\Db\RoleMapper;
use OCA\ISDN\Service\PermissionService;

/**
 * @extends AService<Role,RoleMapper>
 */
class RoleService extends AService
{
    /** @var IGroupManager */
    private $groupManager;
    /** @var PermissionService */
    private $permissionService;

    public function __construct(
        ILogger $logger,
        RoleMapper $mapper,
        IGroupManager $groupManager,
        PermissionService $permissionService
    ) {
        parent::__construct($logger, $mapper);
        $this->groupManager = $groupManager;
        $this->permissionService = $permissionService;
    }

    /**
     * @param Role $role
     * @return Role
     */
    public function enrich(EEntity $role):EEntity
    {
        foreach ($role->getPermissions() as $id => $level) {
            try {
                $permission = $this->permissionService->findById($id, true);
                if (is_integer($level)) {
                    $permission->setLevel($level);
                }
                $role->addPermission($id, $permission);
            } catch (DoesNotExistException $e) {
                $role->resetUpdatedRelationalFields();
                $role->removePermission($id);
                $this->save($role);
                $this->logger->info("Removed stale entry ".$id
                  ." for role ".$role->getId()." - ".$role->getName());
            }
        }
        $role->resetUpdatedRelationalFields();
        return $role;
    }

    public function hasAnyPermission(?IUser $user) : bool
    {
        if (!is_null($user)) {
            try {
                if ($this->mapper->countByUser($user->getUID()) > 0) {
                    return true;
                }
                foreach ($this->groupManager->getUserGroupIds($user) as $groupID) {
                    if ($this->mapper->countByGroup($groupID) > 0) {
                        return true;
                    }
                }
            } catch (DoesNotExistException $e) {
            } catch (\Exception $e) {
                $this->logger->logException($e, ["app" => Application::ID]);
            }

            if ($this->mapper->countByGroup(Role::GROUP_USERS)) {
                return true;
            }
        }

        if ($this->mapper->countByGroup(Role::GROUP_ANONYMOUS) > 0) {
                return true;
        }
        return false;
    }

    public function hasPermission(?IUser $user, string $permission, int $level) : bool
    {
        if (!is_null($user)) {
            try {
                $p = $this->permissionService->findByUserAndPermission($user->getUID(), $permission);
                if ($p->hasLevel($level)) {
                    return true;
                }
            } catch (DoesNotExistException $e) {
            } catch (\Exception $e) {
                $this->logger->logException($e, ["app" => Application::ID]);
            }
            foreach ($this->groupManager->getUserGroupIds($user) as $groupID) {
                if ($this->hasGroupPermission($groupID, $permission, $level)) {
                    return true;
                }
            }
            if ($this->hasGroupPermission(Role::GROUP_USERS, $permission, $level)) {
                return true;
            }
        }
        $this->logger->debug("Testing Permission: ".Role::GROUP_ANONYMOUS."; ".$permission.'; '.$level);
        if ($this->hasGroupPermission(Role::GROUP_ANONYMOUS, $permission, $level)) {
            $this->logger->debug("Has Permission: ".Role::GROUP_ANONYMOUS."; ".$permission.'; '.$level);
            return true;
        }
        return false;
    }

    private function hasGroupPermission(string $groupID, string $permission, int $level) : bool
    {
        try {
            $p = $this->permissionService->findByGroupAndPermission($groupID, $permission);
            if ($p->hasLevel($level)) {
                return true;
            }
        } catch (DoesNotExistException $e) {
        } catch (\Exception $e) {
            $this->logger->logException($e, ["app" => Application::ID]);
        }
        return false;
    }
}
