<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\Mail\IMailer;
use OCP\Mail\IMessage;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\Mail;
use OCA\ISDN\Db\EEntity;
use OCA\ISDN\Db\MailMapper;

/**
 * @extends AService<Mail,MailMapper>
 */
class MailService extends AService
{

    /** @var IMailer */
    protected $mailer;
    /** @var MailTemplateService */
    protected $mailTemplateService;

    public function __construct(
        ILogger $logger,
        MailMapper $mapper,
        IMailer $mailer,
        MailTemplateService $mailTemplateService
    ) {
        parent::__construct($logger, $mapper);
        $this->mailer = $mailer;
        $this->mailTemplateService = $mailTemplateService;
    }

    /**
     * @param Mail $mail
     * @return Mail
     */
    public function enrich(EEntity $mail):EEntity
    {
        $template = $mail->getTemplate();
        if (is_numeric($template)) {
            try {
                $mail->setTemplate($this->mailTemplateService->findById($template, true));
            } catch (DoesNotExistException $e) {
                $this->logger->info("Failed to get mail template ".$template);
            }
        }
        $mail->resetUpdatedFields();
        return $mail;
    }

    /**
     * @return array<Mail>
     */
    public function findUnsendMails() : array
    {
        return $this->mapper->findUnsendMails();
    }

    public function sendMail(Mail $mail) : void
    {
        $template = $mail->getTemplate();
        if (is_numeric($template)) {
            $template = $this->mailTemplateService->createMailerTemplateById(intval($template));
        } else {
            $template = $this->mailTemplateService->createMailerTemplate($template);
        }
        $template->setVars($mail->getVars());

        $message = $this->mailer->createMessage();
        $message->setFrom($template->getFrom());
        $message->setTo(explode(",", $mail->getTo()));
        $message->useTemplate($template);
        try {
             $this->mailer->send($message);
             $mail->setSendAt(new \DateTime());
             $this->save($mail);
        } catch (\Exception $e) {
            $this->logger->error("Failed to send mail ".$mail->getId(), ["exception"=>$e,"app"=>Application::ID]);
            $this->logger->logException($e, ["app"=>Application::ID]);
        }
    }
}
