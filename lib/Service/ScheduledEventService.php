<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\EEntity;
use OCA\ISDN\Db\Event;
use OCA\ISDN\Db\ScheduledEvent;
use OCA\ISDN\Db\ScheduledEventMapper;

/**
 * @extends AService<ScheduledEvent,ScheduledEventMapper>
 */
class ScheduledEventService extends AService
{

    /** @var EventService */
    protected $eventService;

    public function __construct(
        ILogger $logger,
        ScheduledEventMapper $mapper,
        EventService $eventService
    ) {
        parent::__construct($logger, $mapper);
        $this->eventService = $eventService;
    }

    /**
     * @param ScheduledEvent $scheduledEvent
     * @return ScheduledEvent
     */
    public function enrich(EEntity $scheduledEvent):EEntity
    {
        foreach ($scheduledEvent->getEvents() as $id => $maker) {
            try {
                $event = $this->eventService->findById($id, true);
                $scheduledEvent->addEvent($event);
            } catch (DoesNotExistException $e) {
                $scheduledEvent->resetUpdatedRelationalFields();
                $scheduledEvent->removeEvent($id);
                $this->save($scheduledEvent);
                $this->logger->info("Removed stale entry ".$id
                  ." for scheduled event ".$scheduledEvent->getId()." - ".$scheduledEvent->getName());
            }
        }
        $scheduledEvent->resetUpdatedRelationalFields();
        return $scheduledEvent;
    }

    /**
     * @param integer $id
     * @return ScheduledEvent
     */
    public function markAsDeleted(int $id) : ScheduledEvent
    {
        $entity = parent::markAsDeleted($id);
        foreach ($entity->getEvents() as $eventId => $event) {
            $this->eventService->markAsDeleted($eventId);
        }
        return $entity;
    }

    /**
     * @param integer $id
     * @return ScheduledEvent
     */
    public function removeDeletionMark(int $id) : ScheduledEvent
    {
        $entity = parent::removeDeletionMark($id);
        foreach ($entity->getEvents() as $eventId => $event) {
            $this->eventService->removeDeletionMark($eventId);
        }
        return $entity;
    }

    /**
     * @param ScheduledEvent $entity
     * @return ScheduledEvent
     */
    protected function update(EEntity $entity) : EEntity
    {
        $entity = parent::update($entity);
        $entity = $this->deleteFeatureEvents($entity);
        $entity = $this->generateEvents($entity);
        return $entity;
    }

    /**
     * @param ScheduledEvent $entity
     * @return ScheduledEvent
     */
    protected function create(EEntity $entity) : EEntity
    {
        $entity = parent::create($entity);
        $entity = $this->generateEvents($entity);
        return $entity;
    }

    /**
     * @param ScheduledEvent $entity
     * @return ScheduledEvent
     */
    public function delete(EEntity $entity) : EEntity
    {
        $entity = parent::delete($entity);
        $entity = $this->deleteFeatureEvents($entity);
        return $entity;
    }

    protected function deleteFeatureEvents(ScheduledEvent $entity) : ScheduledEvent
    {
        foreach ($entity->getEvents() as $key => $event) {
            if (!($event instanceof Event)) {
                $event = $this->eventService->findById($key, true);
            }
            if ($event->getEnd() > new \DateTime()) {
                $this->eventService->delete($event);
                $entity->removeEvent($event);
            }
        }
        return parent::update($entity);
    }

    protected function generateEvents(ScheduledEvent $entity) : ScheduledEvent
    {
        $rrule = new \Recurr\Rule($entity->getSchedule());
        $config = new \Recurr\Transformer\ArrayTransformerConfig();
        $config->setVirtualLimit(10);
        $transform = new \Recurr\Transformer\ArrayTransformer($config);
        $constraint = null;
        if (!is_null($entity->getLastGenerated())) {
            $constraint = new \Recurr\Transformer\Constraint\BeforeConstraint($entity->getLastGenerated());
        }
        $duration = strptime($entity->getDuration(), "%H:%M");
        if ($duration !== false) {
            $duration = new \DateInterval('PT'.($duration['tm_hour']*3600+$duration['tm_min']*60).'S');
            foreach ($transform->transform($rrule, $constraint) as $recurrence) {
                $start = $recurrence->getStart();
                if ($start instanceof \DateTime) {
                    $event = new Event();
                    $event->setName($entity->getName());
                    $event->setStart($start);
                    $event->setEnd($start->add($duration));
                    $event->setAddress($entity->getAddress());
                    $this->eventService->save($event);
                    $entity->addEvent($event);
                    $entity->setLastGenerated($start);
                }
            }
            $entity = parent::update($entity);
        }
        return $entity;
    }
}
