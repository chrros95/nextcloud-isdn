<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\Permission;
use OCA\ISDN\Db\PermissionMapper;

/**
 * @extends AService<Permission,PermissionMapper>
 */
class PermissionService extends AService
{

    public function __construct(
        ILogger $logger,
        PermissionMapper $mapper
    ) {
        parent::__construct($logger, $mapper);
    }

    public function findByUserAndPermission(string $userId, string $permissionName) : Permission
    {
        return $this->mapper->findByUserAndPermission($userId, $permissionName);
    }

    public function findByGroupAndPermission(string $groupId, string $permissionName) : Permission
    {
        return $this->mapper->findByGroupAndPermission($groupId, $permissionName);
    }
}
