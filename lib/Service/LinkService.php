<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\Link;
use OCA\ISDN\Db\LinkMapper;

/**
 * @extends AService<Link,LinkMapper>
 */
class LinkService extends AService
{

    public function __construct(
        ILogger $logger,
        LinkMapper $mapper
    ) {
        parent::__construct($logger, $mapper);
    }
}
