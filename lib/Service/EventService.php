<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\Event;
use OCA\ISDN\Db\EventMapper;
use OCA\ISDN\Db\CheckIn;
use OCA\ISDN\Db\Person;
use OCA\ISDN\Db\EEntity;

/**
 * @extends AService<Event,EventMapper>
 */
class EventService extends AService
{
    /** @var CheckInService */
    protected $checkInService;

    public function __construct(
        ILogger $logger,
        EventMapper $mapper,
        CheckInService $checkInService
    ) {
        parent::__construct($logger, $mapper);
        $this->checkInService = $checkInService;
    }

    /**
     * @param Event $event
     * @return Event
     */
    public function enrich(EEntity $event):EEntity
    {
        $checkIns = $this->checkInService->findByEvent($event, true);
        $event->setCheckIns($checkIns);
        return $event;
    }

    /**
     * @return array<Event>
     */
    public function findCurrentEvents():array
    {
        return $this->mapper->findCurrentEvents();
    }
}
