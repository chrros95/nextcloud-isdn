<?php
namespace OCA\ISDN\Service;

use OCP\ILogger;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Db\CheckIn;
use OCA\ISDN\Db\CheckInMapper;
use OCA\ISDN\Db\Event;
use OCA\ISDN\Db\EEntity;
use OCA\ISDN\Service\PersonService;

/**
 * @extends AService<CheckIn,CheckInMapper>
 */
class CheckInService extends AService
{
    /** @var PersonService */
    protected $personService;

    public function __construct(
        ILogger $logger,
        CheckInMapper $mapper,
        PersonService $personService
    ) {
        parent::__construct($logger, $mapper);
        $this->personService = $personService;
    }

    /**
     * @param CheckIn $entity
     * @return CheckIn
     */
    public function enrich(EEntity $entity):EEntity
    {
        $person = $entity->getPerson();
        $person = $this->personService->findById(is_numeric($person) ? $person : $person->getId(), true);
        $entity->setPerson($person);
        return $entity;
    }

    /**
     * @return array<CheckIn>
     */
    public function findByEvent(Event $event, bool $enrich = true):array
    {
        $entities = $this->mapper->findByEvent($event);
        if ($enrich) {
            foreach ($entities as $entity) {
                $entity = $this->enrich($entity);
            }
        }
        return $entities;
    }
}
