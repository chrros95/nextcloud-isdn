<?php
namespace OCA\ISDN\Service;

use \PragmaRX\Google2FA\Google2FA;
use OCP\ILogger;
use OCP\IConfig;
use OCP\Http\Client\IClient;
use OCP\Http\Client\IClientService;
use OCP\Http\Client\IResponse;
use OCP\AppFramework\Db\DoesNotExistException;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Exception\ConfigValueMissingException;
use OCA\ISDN\Db\Person;
use OCA\ISDN\Db\Contact;
use OCA\ISDN\Service\PersonService;

class SEWOBEService
{
    /** @var ILogger */
    protected $logger;
    /** @var IConfig */
    protected $config;
    /** @var Google2FA */
    protected $google2FA;
    /** @var IClient */
    protected $client;
    /** @var PersonService */
    protected $personService;


    public function __construct(
        ILogger $logger,
        IConfig $config,
        IClientService $clientService,
        PersonService $personService
    ) {
        $this->logger = $logger;
        $this->config = $config;
        $this->google2FA = new Google2FA();
        $this->client = $clientService->newClient();
        $this->personService = $personService;
    }


    /**
     * @return array<Person>
     */
    public function sync() : array
    {
        $this->login();
        $response = $this->client->post(
            $this->buildUrl("/module/adressen/suche.php"),
            $this->mergeWithDefaultOptions(array(
                "body" => [
                    "SESSION" => $this->getSessionId(),
                    "STICHWORT" => "",
                    "EDIT_MODE" => "0",
                    "FILTER" => "0",
                    "HKAT_ID" => "0",
                    "UKAT_ID" => "0",
                    "AKTION_ID" => "0",
                    "SEITE" => "1",
                    "MAX_TREFFER" => "1000",
                    "SORTIERUNG" => "0",
                    "SELECT_MODE" => "0",
                    "MGM_VERWEIS" => "0",
                    "FUNCTION_AUFRUF" => "0",
                    "BATCH_MODE" => "0",
                    "BATCH_MODE_ID" => "0",
                    "TABELLE_ID" => "0",
                ]
            ))
        );
        $responseBody =  $this->validateResponse($response);
        $persons = $this->parsePersons($responseBody);
        foreach ($persons as $key => $person) {
            $refs = $person->getReferences();
            try {
                $dbPerson = $this->personService->findByReference(Person::REFERENCE_DN, $refs[Person::REFERENCE_DN]);
                $dbPerson->setSurename($person->getSurename());
                $dbPerson->setForename($person->getForename());
                $dbPerson->setBirthday($person->getBirthday());
                foreach ($person->getContacts() as $contact) {
                    $dbPerson->addContact($contact);
                }
                $dbPerson->addReference(Person::REFERENCE_DN, $refs[Person::REFERENCE_DN]);
                $persons[$key] = $this->personService->save($dbPerson);
            } catch (DoesNotExistException $e) {
                $persons[$key] = $this->personService->save($person);
            }
        }
        return $persons;
    }

    /**
     * @return array<Person>
     */
    private function parsePersons(string $responseBody) : array
    {
        $html = new \DOMDocument();
        $html->loadHTML($responseBody);
        $element = $html->getElementById("liste");
        if (is_null($element)) {
            throw new \Exception("Invalid Content. Content:".$responseBody);
        }
        $rows = null;
        foreach ($element->childNodes as $childNode) {
            if ($childNode->nodeName ==="tbody") {
                if ($childNode->hasChildNodes()) {
                    $rows = $childNode->childNodes;
                }
                break;
            }
        }
        if (!$rows || $rows->count() < 4) {
            throw new \Exception("Expected at least 4 rows.");
        }
        $result = [];
        for ($i = 0; $i<$rows->count()-1; $i+=3) {
            $currentRow = $rows->item($i+1);
            $person = new Person();
            if ($currentRow) {
                $currentRow = $currentRow->childNodes;
                $node = $currentRow->item(2);
                if ($node) {
                    $person->addReference(Person::REFERENCE_DN, $node->textContent);
                }
                $node = $currentRow->item(4);
                if ($node) {
                    $names = explode(",", $node->textContent);
                    $person->setSurename($this->clearString($names[0]));
                    $person->setForename($this->clearString(isset($names[1]) ? $names[1] : ""));
                    $node = $node->firstChild;
                    if ($node) {
                        $node = $node->attributes;
                        if ($node) {
                            $node = $node->getNamedItem("href");
                            $node = $node ? $node->textContent : "";
                            if (preg_match('/\((\d+)\)/m', $node, $details) !== false) {
                                $person->addReference(Person::REFERENCE_SN, $details[1]);
                            }
                        }
                    }
                }
                $node = $currentRow->item(6);
                if ($node && $this->clearString($node->textContent)) {
                    $person->addContact(new Contact(
                        "Anschrift",
                        Contact::ADDRESS,
                        $this->clearString($node->textContent)
                    ));
                }
            }
            $currentRow = $rows->item($i+2);
            if ($currentRow) {
                $currentRow = $currentRow->childNodes;
                $node = $currentRow->item(6);
                if ($node && $this->clearString($node->textContent)) {
                    $person->addContact(new Contact(
                        "Home",
                        Contact::TELEFPHONE,
                        $this->clearString($node->textContent)
                    ));
                }
                $node = $currentRow->item(8);
                if ($node && $this->clearString($node->textContent)) {
                    $person->addContact(new Contact(
                        "Fax",
                        Contact::TELEFPHONE,
                        $this->clearString($node->textContent)
                    ));
                }
                $node = $currentRow->item(10);
                if ($node && $this->clearString($node->textContent)) {
                    $person->addContact(new Contact(
                        "Mobil",
                        Contact::TELEFPHONE,
                        $this->clearString($node->textContent)
                    ));
                }
                $node = $currentRow->item(12);
                if ($node && $this->clearString($node->textContent)) {
                    $person->addContact(new Contact(
                        "E-Mail",
                        Contact::EMAIL,
                        $this->clearString($node->textContent)
                    ));
                }
            }
            $result[] = $person;
        }
        return $result;
    }

    private function clearString(string $s): string
    {
        $s = trim($s);
        $tmp = preg_replace('/\xc2\xa0/', '', $s);
        return $tmp ? $tmp : $s;
    }

    private function login() : void
    {
        $params = array();
        foreach (["secret", "domain", "username", "password", "mandant"] as $key) {
            $params[$key] = $this->config->getAppValue(Application::ID, "sewobe_".$key);
            if (!$params[$key]) {
                throw new ConfigValueMissingException("Config Value for 'sewobe_".$key."' is missing.");
            }
        }
        $status = $this->isAuthenticated();
        if ($status == 0) {
            return;
        } elseif ($status == 2) {
            $this->handleLoginForm($params["username"], $params["password"], $params["mandant"]);
        }
        $this->handle2FA($params["secret"]);
    }

    private function handleLoginForm(string $username, string $password, string $mandant) : void
    {
        $loginURL = $this->buildUrl("/module/login.php");
        $response = $this->client->get($loginURL, $this->mergeWithDefaultOptions());
        $responseBody =  $this->validateResponse($response);
        $html = new \DOMDocument();
        $html->loadHTML($responseBody);
        $loginSessionId = $html->getElementById("LOGINSESSION");
        if (is_null($loginSessionId)) {
            throw new \Exception("Failed to get login session id. Content:".$responseBody);
        }
        $loginSessionId = $loginSessionId->getAttribute("value");
        $this->logger->debug("LoginSessionId ". $loginSessionId);
        $response = $this->client->post($loginURL, $this->mergeWithDefaultOptions(array(
            'body' => [
                'USERNAME' => $username,
                'PASSWORT' => $password,
                'LOGINSESSION' => $loginSessionId,
                'EDIT_MODE' => "1",
                'MANDANT_KEY' => $mandant,
                'ROUTEN_TYP' => "0",
                'ROUTEN_ID' => "0"
            ]
        )));
        $responseBody =  $this->validateResponse($response);
        if (preg_match('/href\s=\s"([^?]*)\?SESSION=([^"]*)"/m', $responseBody, $loginParams) === false) {
            throw new \Exception("Unable to find login redirect.");
        }
        $this->config->setAppValue(Application::ID, "sewobe_sessionid", $loginParams[2]);
    }

    private function handle2FA(string $secret) : void
    {
        $response = $this->client->post(
            $this->buildUrl("/module/zweifalogin.php"),
            $this->mergeWithDefaultOptions(array(
            "body" => [
                "2FCODE" => $this->google2FA->getCurrentOtp($secret),
                "BEARB_MODE" => "2",
                "SESSION" => $this->getSessionId()
                ]
            ))
        );
        $responseBody =  $this->validateResponse($response);
    }

    private function getSessionId():string
    {
        return $this->config->getAppValue(Application::ID, "sewobe_sessionid", "");
    }

    /**
     * @param array<mixed>|null $params
     * @return array<mixed>
     */
    private function mergeWithDefaultOptions($params = null) : array
    {
        $domain = $this->config->getAppValue(Application::ID, "sewobe_domain");
        $defaults = array("debug" => false);
        $cookies = array("SESSION" => $this->getSessionId());
        if (!$cookies["SESSION"]) {
            unset($cookies["SESSION"]);
        }
        if (isset($params["cookies"]) && is_array($params["cookies"])) {
            $cookies = array_merge($cookies, $params["cookies"]);
            unset($params["cookies"]);
        }
        $defaults["cookies"] = \GuzzleHttp\Cookie\CookieJar::fromArray($cookies, $domain);
        if (is_null($params)) {
            return $defaults;
        }
        return array_merge_recursive($defaults, $params);
    }

    private function isAuthenticated(?IResponse $response = null) : int
    {
        if (is_null($response)) {
            $response = $this->client->get(
                $this->buildUrl("/module/allgemein/main.php"),
                $this->mergeWithDefaultOptions()
            );
        }
        $responseBody =  $this->validateResponse($response);
        if (!str_contains($responseBody, "location.href")) {
            return 0;
        } elseif (str_contains($responseBody, "zweifalogin.php")) {
            return 1;
        } else {
            $this->config->setAppValue(Application::ID, "sewobe_sessionid", "");
            return 2;
        }
    }

    private function buildUrl(string $path) : string
    {
        $baseUrl = "https://".$this->config->getAppValue(Application::ID, "sewobe_domain");
        return $baseUrl.(!str_starts_with($path, "/") ? "/" : "").$path;
    }

    private function validateResponse(IResponse $response, int $expectedStatusCode = 200) : string
    {
        $responseBody = $response->getBody();
        if (is_resource($responseBody)) {
            throw new \Exception("HTTP Request returned ressource instead of a string. ".print_r($response, true));
        }
        if ($response->getStatusCode() != $expectedStatusCode) {
            $this->logger->error(
                "Unexpected Status Code ".$response->getStatusCode()."(expected: ".$expectedStatusCode."): "
                . $responseBody
            );
            throw new \Exception("HTTP Request Failed".print_r($response, true));
        }
        return $responseBody;
    }
}
