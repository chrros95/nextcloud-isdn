<?php
namespace OCA\ISDN\Db;

/**
 * @method void setLabel(string $s)
 * @method void setName(string $s)
 * @method void setSubject(string $s)
 * @method void setFrom(string $s)
 * @method string getLabel()
 * @method string getName()
 * @method string getSubject()
 * @method string getFrom()
 */
class MailTemplate extends EEntity
{
    /** @var string */
    protected $label;
    /** @var string */
    protected $name;
    /** @var string */
    protected $subject;
    /** @var string */
    protected $from;

    public function __construct(
        ?string $label = null,
        ?string $name = null,
        ?string $subject = null,
        ?string $from = null
    ) {
        parent::__construct();
        if (!is_null($label)) {
            $this->setLabel($label);
        }
        if (!is_null($name)) {
            $this->setName($name);
        }
        if (!is_null($subject)) {
            $this->setSubject($subject);
        }
        if (!is_null($from)) {
            $this->setFrom($from);
        }
    }
}
