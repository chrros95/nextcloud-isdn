<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\AppInfo\Application;

/**
 * @extends EQBMapper<MailTemplate>
 */
class MailTemplateMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'mailtemplate', MailTemplate::class);
    }

    /**
     * @param mixed $args
     */
    public function find(...$args):EEntity
    {
        if (count($args) !== 1 || gettype($args[0]) !== "string") {
            throw new \Exception("Invalid Param. Count:".count($args)."; Type: ".gettype($args[0]));
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("label", $qb->createNamedParameter($args[0]))
            );
        return $this->findEntity($qb);
    }
}
