<?php
namespace OCA\ISDN\Db;

use OCP\IUser;
use OCP\IGroup;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCA\ISDN\AppInfo\Application;

/**
 * @method void setName(string $s)
 * @method void setUsers(array<int> $s)
 * @method void setGroups(array<int> $s)
 * @method void setPermissions(array<int> $s)
 * @method string getName()
 * @method array<int> getUsers()
 * @method array<int> getGroups()
 * @method array<int,int|Permission> getPermissions()
 */
class Role extends EEntity
{
    /** @var string */
    const GROUP_USERS = Application::ID."_users";
    /** @var string */
    const GROUP_ANONYMOUS = Application::ID."_anonymous";

    /** @var string */
    protected $name;
    /** @var array<int> */
    protected $users = [];
    /** @var array<int> */
    protected $groups = [];
    /** @var array<int|Permission> */
    public $permissions = [];

    public function __construct(?string $name = null)
    {
        parent::__construct();
        $this->addRelationalField("users", IQueryBuilder::PARAM_STR);
        $this->addRelationalField("groups", IQueryBuilder::PARAM_STR);
        $this->addRelationalField("permissions");
        if (!is_null($name)) {
            $this->setName($name);
        }
    }

    /**
     * Set the permissions.
     * See \OCP\Constants::PERMISSION_* concat permissions with +
     * @param int|Permission $value
     */
    public function addPermission(int $id, $value):void
    {
        if (!isset($this->permissions[$id])) {
            $this->markRelationalFieldUpdated("permissions", $id, 1);
        } elseif ($this->permissions[$id] != $value) {
            $this->markRelationalFieldUpdated("permissions", $id, 2);
        }
        $this->permissions[$id] = $value;
    }

    public function removePermission(int $id):void
    {
        unset($this->permissions[$id]);
        $this->markRelationalFieldUpdated("permissions", $id);
    }

    /**
     * @param IUser|IGroup|string $owner
     */
    public function addOwner($owner, ?string $type = null):void
    {
        if ($owner instanceof IGroup || (is_string($owner) && $type == "group" )) {
            if ($owner instanceof IGroup) {
                $owner = $owner->getGID();
            }
            if (!isset($this->groups[$owner]) || $this->groups[$owner] != 1) {
                $this->markRelationalFieldUpdated("groups", $owner, 1);
            }
            $this->groups[$owner] = 1;
        } elseif ($owner instanceof IUser || (is_string($owner) && $type == "user" )) {
            if ($owner instanceof IUser) {
                $owner = $owner->getUID();
            }
            if (!isset($this->users[$owner]) || $this->users[$owner] != 1) {
                $this->markRelationalFieldUpdated("users", $owner, 1);
            }
            $this->users[$owner] = 1;
        }
    }

    /**
     * @param IUser|IGroup|string $owner
     * @param string|null $type
     */
    public function removeOwner($owner, ?string $type = null):void
    {
        if ($owner instanceof IGroup || (is_string($owner) && $type == "group" )) {
            if ($owner instanceof IGroup) {
                $owner = $owner->getGID();
            }
            unset($this->groups[$owner]);
            $this->markRelationalFieldUpdated("groups", $owner);
        } elseif ($owner instanceof IUser || (is_string($owner) && $type == "user" )) {
            if ($owner instanceof IUser) {
                $owner = $owner->getUID();
            }
            unset($this->users[$owner]);
            $this->markRelationalFieldUpdated("users", $owner);
        }
    }
}
