<?php
namespace OCA\ISDN\Db;

/**
 * @method void setLabel(string $s)
 * @method void setType(int $s)
 * @method void setData(string $s)
 * @method string getLabel()
 * @method int getType()
 * @method string getData()
 */
class Contact extends EEntity
{
    /** @var int Nexclout */
    const TELEFPHONE = 0;
    /** @var int DLRG ISC */
    const ADDRESS = 1;
    /** @var int DLRG-Manager/Sewobe */
    const EMAIL = 2;

    /** @var string */
    protected $label;
    /** @var int */
    protected $type;
    /** @var string */
    protected $data;

    public function __construct(?string $label = null, ?int $type = null, ?string $data = null)
    {
        parent::__construct();
        $this->addInternalType('type', 'integer');
        //$this->addInternalType('data', 'json');
        if (!is_null($label)) {
            $this->setLabel($label);
        }
        if (!is_null($type)) {
            $this->setType($type);
        }
        if (!is_null($data)) {
            $this->setData($data);
        }
    }

    /**
     * @return mixed
     */
    public function getRelationalValue()
    {
        return json_encode($this);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public static function fromRelationValue($value)
    {
        $json = json_decode($value);
        return new Contact($json->label, $json->type, $json->data);
    }
}
