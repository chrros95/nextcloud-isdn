<?php
namespace OCA\ISDN\Db;

/**
 * @method void setName(string $s)
 * @method void setSchedule(string $s)
 * @method void setDuration(string $s)
 * @method void setAddress(string $s)
 * @method void setEvents(array<Event> $s)
 * @method void setLastGenerated(int|\DateTime $s)
 * @method string getName()
 * @method string getSchedule()
 * @method string getDuration()
 * @method Contact getAddress()
 * @method array<Event> getEvents()
 * @method \DateTime|null getLastGenerated()
 */
class ScheduledEvent extends EEntity
{
    /** @var string */
    protected $name;
    /** @var string */
    protected $schedule;
    /** @var string */
    protected $duration;
    /** @var Contact */
    protected $address;
    /** @var array<Event|null> */
    protected $events;
    /** @var int|null */
    protected $lastGenerated = null;

    public function __construct()
    {
        parent::__construct();
        $this->addInternalType('lastGenerated', 'date');
        $this->addRelationalField("events");
    }

    /**
     * @param Event $event
     */
    public function addEvent(Event $event):void
    {
        $id = $event->getId();
        if (!isset($this->events[$id])) {
            $this->markRelationalFieldUpdated("events", $id, 1);
        }
        $this->events[$id] = $event;
    }

    /**
     * @param int|Event $event
     */
    public function removeEvent($event):void
    {
        if ($event instanceof Event) {
            $id = $event->getId();
        } else {
            $id = $event;
        }
        unset($this->events[$id]);
        $this->markRelationalFieldUpdated("events", $id);
    }
}
