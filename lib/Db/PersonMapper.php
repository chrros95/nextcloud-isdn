<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\AppInfo\Application;

/**
 * @extends EQBMapper<Person>
 */
class PersonMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'person', Person::class);
    }

    /**
     * @params mixed $args
     */
    public function find(...$args):EEntity
    {
        if (count($args) != 2 || gettype($args[0]) !== "string" || gettype($args[1]) !== "string") {
            throw new \Exception("Invalid Param");
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("forename", $qb->createNamedParameter($args[0])),
                $qb->expr()->eq("surename", $qb->createNamedParameter($args[1]))
            );
        return $this->findEntity($qb);
    }

    public function findByReference(string $type, string $id) : Person
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('p.id as id', "forename", "surename", "birthday")
            ->from($this->getTableName()."_r", "pr")
            ->leftJoin("pr", $this->getTableName(), "p", "p.id = pr.id")
            ->where(
                $qb->expr()->eq("pr.rid", $qb->createNamedParameter($type), IQueryBuilder::PARAM_STR),
                $qb->expr()->eq("pr.value", $qb->createNamedParameter($id), IQueryBuilder::PARAM_STR)
            );
        return $this->findEntity($qb);
    }
}
