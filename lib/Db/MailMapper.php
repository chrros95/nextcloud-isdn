<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\AppInfo\Application;

/**
 * @extends EQBMapper<Mail>
 */
class MailMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'mail', Mail::class);
    }

    /**
     * @param mixed $args
     */
    public function find(...$args):EEntity
    {
        if (count($args) !== 2 || gettype($args[0]) !== "integer" || gettype($args[1]) !== "string") {
            throw new \Exception("Invalid Param");
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("send_at", $qb->createNamedParameter($args[0]), IQueryBuilder::PARAM_INT),
                $qb->expr()->like("to", $qb->createNamedParameter("%".$args[1]."%"))
            );
        return $this->findEntity($qb);
    }

    /**
     * @return array<Mail>
     */
    public function findUnsendMails():array
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->isNull("send_at")
            );
        return $this->findEntities($qb);
    }
}
