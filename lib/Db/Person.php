<?php
namespace OCA\ISDN\Db;

use OCP\DB\QueryBuilder\IQueryBuilder;

/**
 * @method void setForename(string $s)
 * @method void setSurename(string $s)
 * @method void setBirthday(\DateTime $s)
 * @method void setContacts(array<Contact> $s)
 * @method void setReferences(array<string> $s)
 * @method string getForename()
 * @method string getSurename()
 * @method \DateTime getBirthday()
 * @method array<Contact> getContacts()
 * @method array<string> getReferences()
 */
class Person extends EEntity
{
    /** @var string Nexclout */
    const REFERENCE_NC = "NC";
    /** @var string DLRG ISC */
    const REFERENCE_ISC = "ISC";
    /** @var string DLRG-Manager */
    const REFERENCE_DN = "MV";
    /** @var string DLRG-Manager/Sewobe */
    const REFERENCE_SN = "SE";

    /** @var string */
    protected $forename;
    /** @var string */
    protected $surename;
    /** @var int */
    protected $birthday;
    /** @var array<mixed> */
    public $contacts = [];
    /** @var array<string> */
    public $references = [];

    public function __construct(?string $forename = null)
    {
        parent::__construct();
        $this->addInternalType('birthday', 'date');
        $this->addRelationalField("contacts", IQueryBuilder::PARAM_STR, IQueryBuilder::PARAM_STR, Contact::class);
        $this->addRelationalField("references", IQueryBuilder::PARAM_STR);
        if (!is_null($forename)) {
            $this->setForename($forename);
        }
    }

    public function addReference(string $type, string $value):void
    {
        if (!isset($this->references[$type])) {
            $this->markRelationalFieldUpdated("references", $type, 1);
        } elseif ($this->references[$type] != $value) {
            $this->markRelationalFieldUpdated("references", $type, 2);
        }
        $this->references[$type] = $value;
    }

    public function removeReference(string $type):void
    {
        unset($this->references[$type]);
        $this->markRelationalFieldUpdated("references", $type);
    }

    /**
     * @param Contact $contact
     */
    public function addContact(Contact $contact):void
    {
        $label = $contact->getLabel();
        if (!isset($this->contacts[$label])) {
            $this->markRelationalFieldUpdated("contacts", $label, 1);
        } elseif ($this->contacts[$label] != $contact) {
            $this->markRelationalFieldUpdated("contacts", $label, 2);
        }
        $this->contacts[$label] = $contact;
    }

    /**
     * @param string|Contact $contact
     */
    public function removeContact($contact):void
    {
        if ($contact instanceof Contact) {
            $label = $contact->getLabel();
        } else {
            $label = $contact;
        }
        unset($this->contacts[$label]);
        $this->markRelationalFieldUpdated("contacts", $label);
    }
}
