<?php
namespace OCA\ISDN\Db;

/**
 * @method void setName(string $s)
 * @method void setLevel(int $s)
 * @method string getName()
 * @method int getLevel()
 */
class Permission extends EEntity
{

    /** @var string */
    protected $name;
    /** @var int */
    protected $level;

    public function __construct(?string $name = null)
    {
        parent::__construct();
        $this->addInternalProperty("level", false, true);
        if (!is_null($name)) {
            $this->setName($name);
        }
    }

    /**
     * @return int
     */
    public function getRelationalValue()
    {
        return $this->getLevel();
    }

    public function hasLevel(int $level) : bool
    {
        return ($this->getLevel() & $level) === $level;
    }
}
