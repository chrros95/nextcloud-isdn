<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\AppInfo\Application;

/**
 * @extends EQBMapper<Event>
 */
class EventMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'event', Event::class);
    }

    /**
     * @params mixed $args
     */
    public function find(...$args):EEntity
    {
        if (count($args) > 1 || gettype($args[0]) !== "string") {
            throw new \Exception("Invalid Param");
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("name", $qb->createNamedParameter($args[0]))
            );
        return $this->findEntity($qb);
    }

    /**
     * @return array<Event>
     */
    public function findCurrentEvents():array
    {
        $now = time();
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->lte('start', $qb->createNamedParameter($now), IQueryBuilder::PARAM_INT),
                $qb->expr()->gte('end', $qb->createNamedParameter($now), IQueryBuilder::PARAM_INT)
            );
        return $this->findEntities($qb);
    }
}
