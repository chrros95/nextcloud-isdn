<?php
namespace OCA\ISDN\Db;

/**
 * @method void setName (string $s)
 * @method void setStart (\DateTime $s)
 * @method void setEnd (\DateTime $s)
 * @method void setAddress (string|Contact $s)
 * @method void setCheckIns (array<CheckIn> $s)
 * @method string getName()
 * @method \DateTime getStart()
 * @method \DateTime getEnd()
 * @method Contact getAddress()
 * @method array<CheckIn> getCheckIns()
 */
class Event extends EEntity
{
    /** @var string */
    protected $name;
    /** @var int */
    protected $start;
    /** @var int */
    protected $end;
    /** @var Contact */
    protected $address;
    /** @var array<CheckIn> */
    protected $checkIns;

    public function __construct()
    {
        parent::__construct();
        $this->addInternalType('start', 'date');
        $this->addInternalType('end', 'date');
        $this->addInternalProperty("checkIns", false, true);
    }
}
