<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\ISDN\AppInfo\Application;

/**
 * @extends EQBMapper<CheckIn>
 */
class CheckInMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'checkin', CheckIn::class);
    }

    /**
     * @params mixed $args
     */
    public function find(...$args):EEntity
    {
        if (count($args) != 2 || !is_numeric($args[0]) || !is_numeric($args[1])) {
            throw new \Exception("Invalid Param");
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("person", $qb->createNamedParameter($args[0]), IQueryBuilder::PARAM_INT),
                $qb->expr()->eq("check_in", $qb->createNamedParameter($args[1]), IQueryBuilder::PARAM_INT)
            );
        return $this->findEntity($qb);
    }

    /**
     * @return array<CheckIn>
     */
    public function findByEvent(Event $event):array
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
        ->from($this->getTableName())
        ->where(
            $qb->expr()->eq("event", $qb->createNamedParameter($event->getId()), IQueryBuilder::PARAM_INT),
        );
        return $this->findEntities($qb);
    }
}
