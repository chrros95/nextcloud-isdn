<?php
namespace OCA\ISDN\Db;

use Doctrine\DBAL\Platforms\SqlitePlatform;
use Doctrine\DBAL\Platforms\PostgreSQL94Platform;
use Doctrine\DBAL\Platforms\PostgreSQL100Platform;

use OCP\IDBConnection;
use OCP\AppFramework\Db\QBMapper;
use OCP\AppFramework\Db\Entity;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\ISDN\AppInfo\Application;

/**
 * @template         T of EEntity
 * @template-extends QBMapper<T>
 */
abstract class EQBMapper extends QBMapper
{

    /**
     * @param class-string<T> $class
     */
    public function __construct(IDBConnection $db, string $table, string $class)
    {
        parent::__construct($db, Application::ID.'_'.$table, $class);
    }

    public function delete(Entity $entity): Entity
    {
        $oldKAP = $entity->keepAsPrimary();
        $entity->setKeepAsPrimary(true);
        $entity = parent::delete($entity);
        $this->clearRelationalFields($entity);
        $entity->setKeepAsPrimary($oldKAP);
        return $entity;
    }

    public function insert(Entity $entity): Entity
    {
        $oldKAP = $entity->keepAsPrimary();
        $entity->setKeepAsPrimary(true);
        $entity = parent::insert($entity);
        $this->saveRelationalFields($entity);
        $entity->setKeepAsPrimary($oldKAP);
        return $entity;
    }

    public function update(Entity $entity): Entity
    {
        $oldKAP = $entity->keepAsPrimary();
        $entity->setKeepAsPrimary(true);
        $entity = parent::update($entity);
        $this->saveRelationalFields($entity);
        $entity->setKeepAsPrimary($oldKAP);
        return $entity;
    }

    /**
     * @param  array<mixed> $row
     * @return T
     */
    protected function mapRowToEntity(array $row) : Entity
    {
        $entity = parent::mapRowToEntity($row);
        return $this->loadRelationalFields($entity);
    }

    /**
     * @param  T $entity
     * @return T
     */
    protected function loadRelationalFields($entity)
    {
        $idType = $this->getParameterTypeForProperty($entity, 'id');
        foreach ($entity->getRelationalFields() as $field => $v) {
            $qb = $this->db->getQueryBuilder();
            $qb->select("*")
                ->from($this->getTableName()."_".substr($field, 0, 1))
                ->where(
                    $qb->expr()->eq('id', $qb->createNamedParameter($entity->getId(), $idType))
                );
            $qb = $qb->execute();
            $values = [];
            if (!is_int($qb)) {
                foreach ($qb->fetchAll() as $row) {
                    if (!$v[2]) {
                        $values[$row["rid"]] = $row["value"];
                    } else {
                        $class = $v[2];
                        $values[$row["rid"]] = $class::fromRelationValue($row["value"]);
                    }
                }
                $setter = 'set' . ucfirst($field);
                $entity->$setter($values);
            }
        }
        $entity->resetUpdatedRelationalFields();
        return $entity;
    }


    /**
     * @param  T $entity
     * @return void
     */
    protected function clearRelationalFields($entity):void
    {
        if (!($entity instanceof EEntity)) {
            return;
        }
        $idType = $this->getParameterTypeForProperty($entity, 'id');
        foreach ($entity->getRelationalFields() as $field => $v) {
            $qb = $this->db->getQueryBuilder();
            $qb->delete($this->getTableName()."_".substr($field, 0, 1))
                ->where(
                    $qb->expr()->eq('id', $qb->createNamedParameter($entity->getId(), $idType))
                );
            $qb->execute();
        }
    }

    /**
     * @param  T $entity
     * @return void
     */
    protected function saveRelationalFields(EEntity $entity):void
    {
        $idType = $this->getParameterTypeForProperty($entity, 'id');
        $updatedRelations = $entity->getUpdatedRelationalFields();
        foreach ($entity->getRelationalFields() as $field => $properties) {
            $methodName = "get".ucfirst($field);
            $fieldValues = $entity->$methodName();
            foreach ($updatedRelations[$field] as $key => $value) {
                $qb = $this->db->getQueryBuilder();
                if ($value !== null) {
                    $fieldValue = $fieldValues[$key];
                    if ($fieldValue instanceof EEntity) {
                        $fieldValue = $fieldValue->getRelationalValue();
                    }
                    if ($value == 2) {
                        $qb->update($this->getTableName()."_".substr($field, 0, 1))
                        ->set("value", $qb->createNamedParameter($fieldValue, $properties[1]))
                        ->where(
                            $qb->expr()->eq('id', $qb->createNamedParameter($entity->getId(), $idType)),
                            $qb->expr()->eq('rid', $qb->createNamedParameter($key, $properties[0]))
                        );
                    } else {
                        $qb->insert($this->getTableName()."_".substr($field, 0, 1))
                        ->setValue("id", $qb->createNamedParameter($entity->getId(), $idType))
                        ->setValue("rid", $qb->createNamedParameter($key, $properties[0]))
                        ->setValue("value", $qb->createNamedParameter($fieldValue, $properties[1]));
                    }
                } else {
                    $qb->delete($this->getTableName()."_".substr($field, 0, 1))
                        ->where(
                            $qb->expr()->eq('id', $qb->createNamedParameter($entity->getId(), $idType)),
                            $qb->expr()->eq('rid', $qb->createNamedParameter($key, $properties[0]))
                        );
                }
                $qb->execute();
            }
        }
    }

    /**
     * @param  string $field
     * @param  mixed  $value
     * @param  int    $type
     * @return int
     */
    protected function countBy(string $field, $value, int $type = IQueryBuilder::PARAM_STR, ?string $table = null):int
    {
        if (is_null($table)) {
            $table = $this->getTableName();
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('id')
            ->from($table)
            ->where(
                $qb->expr()->eq($field, $qb->createNamedParameter($value), $type)
            );
        $qb = $qb->execute();
        if (!is_int($qb)) {
            if (!$this->db->getDatabasePlatform() instanceof SqlitePlatform
                && !$this->db->getDatabasePlatform() instanceof PostgreSQL94Platform
            ) {
                return $qb->rowCount();
            } else {
                return count($qb->fetchAll());
            }
        }
        return 0;
    }

    public function clear(?string $table = null):void
    {
        $qb = $this->db->getQueryBuilder();
        if (is_null($table)) {
            $qb->delete($this->getTableName())->execute();
        } else {
            $qb->delete($table)->execute();
        }
    }

    public function exists(int $id) : bool
    {
        if ($this->countBy("id", $id) > 0) {
            return true;
        }
        return false;
    }


    /**
     * @return T
     */
    public function findById(int $id)
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("id", $qb->createNamedParameter($id), IQueryBuilder::PARAM_INT)
            );
        return $this->findEntity($qb);
    }

    /**
     * @param mixed $args
     * @return T
     */
    abstract public function find(...$args):EEntity;

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @param array<array<string>> $orderBy
     * @return array<T>
     */
    public function findAll(
        ?int $limit = null,
        ?int $offset = null,
        ?array $orderBy = null
    ):array {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
        ->from($this->getTableName())
        ->where($qb->expr()->isNull("deleted_at"));
        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }
        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }

        if ($orderBy !== null) {
            foreach ($orderBy as $order) {
                $qb->addOrderBy($order[0], isset($order[1]) ? $order[1] : null);
            }
        }
        return $this->findEntities($qb);
    }

    /**
     * @return array<T>
     */
    public function findAllDeleted():array
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
        ->from($this->getTableName())
        ->where($qb->expr()->isNotNull("deleted_at"));
        return $this->findEntities($qb);
    }
}
