<?php
namespace OCA\ISDN\Db;

/**
 * @method void setPerson(Person $p)
 * @method void setEvent(Event $t)
 * @method void setCheckIn(int|\DateTime $t)
 * @method int|Person getPerson()
 * @method Event getEvent()
 * @method \DateTime getCheckIn()
 */
class CheckIn extends EEntity
{
    /** @var int|Person */
    protected $person;
    /** @var int|Event */
    protected $event;
    /** @var int */
    protected $checkIn;

    public function __construct(?Person $person = null, ?Event $event = null, ?int $checkIn = null)
    {
        parent::__construct();
        $this->addInternalType('checkIn', 'date');
        if (!is_null($person)) {
            $this->setPerson($person);
        }
        if (!is_null($event)) {
            $this->setEvent($event);
        }
        if (!is_null($checkIn)) {
            $this->setCheckIn($checkIn);
        }
    }
}
