<?php
namespace OCA\ISDN\Db;

/**
 * @method void setTo(string $s)
 * @method void setTemplate(int|MailTemplate $s)
 * @method void setVars(string|\JsonSerializable|array<\JsonSerializable> $s)
 * @method void setSendAt(int|\DateTime $s)
 * @method string getTo()
 * @method int|MailTemplate getTemplate()
 * @method array<\JsonSerializable> getVars()
 * @method \DateTime getSendAt()
 */
class Mail extends EEntity
{
    /** @var string */
    protected $to;
    /** @var integer|MailTemplate */
    protected $template;
    /** @var string */
    protected $vars;
    /** @var integer|null */
    protected $sendAt = null;

    public function __construct(
        ?string $to = null,
        ?int $template = null,
        ?\JsonSerializable $vars = null,
        ?int $sendAt = null
    ) {
        parent::__construct();
        $this->addInternalType('sendAt', 'date');
        $this->addInternalType('template', 'integer');
        $this->addInternalType('vars', 'json');

        if (!is_null($to)) {
            $this->setTo($to);
        }
        if (!is_null($template)) {
            $this->setTemplate($template);
        }
        if (!is_null($vars)) {
            $this->setVars($vars);
        }
        if (!is_null($sendAt)) {
            $this->setSendAt($sendAt);
        }
    }
}
