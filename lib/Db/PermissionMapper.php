<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\ISDN\AppInfo\Application;
use OCP\AppFramework\Db\DoesNotExistException;

/**
 * @extends EQBMapper<Permission>
 */
class PermissionMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'permission', Permission::class);
    }

    /**
     * @params mixed $args
     */
    public function find(...$args):EEntity
    {
        if (count($args) > 1 || gettype($args[0]) !== "string") {
            throw new \Exception("Invalid Param");
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("name", $qb->createNamedParameter($args[0]))
            );
        return $this->findEntity($qb);
    }

    public function findByUserAndPermission(string $userId, string $permissionName):Permission
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select("pr.value as level")
            ->from($this->getTableName(), "p")
            ->join("p", Application::ID."_role_p", "pr", $qb->expr()->eq("p.id", "pr.rid"))
            ->leftJoin("pr", Application::ID."_role_u", "ur", $qb->expr()->eq("pr.id", "ur.id"))
            ->where(
                $qb->expr()->eq("p.name", $qb->createNamedParameter($permissionName)),
                $qb->expr()->eq("ur.rid", $qb->createNamedParameter($userId))
            )
            ->orderBy("pr.value", "DESC")
            ->setMaxResults(1);
        $qb = $qb->execute();
        if (is_int($qb)) {
            throw new \Exception("Invalid Integer Result");
        }
        $row = $qb->fetch();
        if (!$row) {
            throw new DoesNotExistException("Permission doesn't exists");
        }
        $entity = new Permission($permissionName);
        $entity->setLevel($row["level"]);
        return $entity;
    }

    public function findByGroupAndPermission(string $groupId, string $permissionName):Permission
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select("pr.value as level")
            ->from(Application::ID."_role_p", "pr")
            ->leftJoin("pr", $this->getTableName(), "p", $qb->expr()->eq("p.id", "pr.rid"))
            ->leftJoin("pr", Application::ID."_role_g", "gr", $qb->expr()->eq("pr.id", "gr.id"))
            ->where(
                $qb->expr()->eq("p.name", $qb->createNamedParameter($permissionName)),
                $qb->expr()->eq("gr.rid", $qb->createNamedParameter($groupId))
            )
            ->orderBy("pr.value", "DESC")
            ->setMaxResults(1);
        $qb = $qb->execute();
        if (is_int($qb)) {
            throw new \Exception("Invalid Integer Result");
        }
        $row = $qb->fetch();
        if (!$row) {
            throw new DoesNotExistException("Permission doesn't exists");
        }
        $entity = new Permission($permissionName);
        $entity->setLevel($row["level"]);
        return $entity;
    }
}
