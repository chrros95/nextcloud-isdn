<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\AppInfo\Application;

/**
 * @extends EQBMapper<Contact>
 */
class ContactMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'contact', Contact::class);
    }

    /**
     * @params mixed $args
     */
    public function find(...$args):EEntity
    {
        if (count($args) > 1 || gettype($args[0]) !== "string") {
            throw new \Exception("Invalid Param");
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("data", $qb->createNamedParameter($args[0]))
            );
        return $this->findEntity($qb);
    }
}
