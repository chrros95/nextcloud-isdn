<?php
namespace OCA\ISDN\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCA\AppInfo\Application;

/**
 * @extends EQBMapper<Role>
 */
class RoleMapper extends EQBMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'role', Role::class);
    }

    /**
     * @param mixed $args
     * @return Role
     */
    public function find(...$args):Role
    {
        if (count($args) > 1 || gettype($args[0]) !== "string") {
            throw new \Exception("Invalid Param");
        }
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where(
                $qb->expr()->eq("name", $qb->createNamedParameter($args[0]))
            );
        return $this->findEntity($qb);
    }

    /**
     * @return int
     */
    public function countByUser(string $userID):int
    {
        return $this->countBy("rid", $userID, IQueryBuilder::PARAM_STR, $this->getTableName()."_u");
    }

    /**
     * @return int
     */
    public function countByGroup(string $groupID):int
    {
        return $this->countBy("rid", $groupID, IQueryBuilder::PARAM_STR, $this->getTableName()."_g");
    }
}
