<?php
namespace OCA\ISDN\Db;

/**
 * @method void setLabel(string $s)
 * @method void setTarget(string $s)
 * @method void setHref(string $s)
 * @method string getLabel()
 * @method string getTarget()
 * @method string getHref()
 */
class Link extends EEntity
{
    /** @var string */
    protected $label;
    /** @var string */
    protected $target;
    /** @var string */
    protected $href;

    public function __construct(?string $label = null, ?string $target = null, ?string $href = null)
    {
        parent::__construct();
        if (!is_null($label)) {
            $this->setLabel($label);
        }
        if (!is_null($target)) {
            $this->setTarget($target);
        }
        if (!is_null($href)) {
            $this->setHref($href);
        }
    }
}
