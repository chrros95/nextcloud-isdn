<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\JSONResponse;
use \OCP\IRequest;
use \OCP\IUserSession;
use \OCP\Constants;

use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\MailService;
use OCA\ISDN\Service\MailTemplateService;
use OCA\ISDN\Db\Mail;

/**
* @extends ARessourceController<\OCA\ISDN\Db\Mail,\OCA\ISDN\Db\MailMapper,MailService>
 */
class MailController extends ARessourceController
{
    /** @var MailService */
    protected $mailService;
    /** @var string */
    protected $permissionName = "Mails";
    /** @var MailTemplateService */
    protected $mailTemplateService;

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        MailService $mailService,
        MailTemplateService $mailTemplateService
    ) {
        parent::__construct($request, $userSession, $roleService, $mailService);
        $this->mailTemplateService = $mailTemplateService;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @param array<string> $to
     * @param array<string> $variables
     * @param array<\JsonSerializable> $variables
     */
    public function create(string $template, array $to, $variables): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_CREATE)) {
            $template = $this->mailTemplateService->find(false, $template);
            $mail = new Mail(implode(",", $to));
            $mail->setTemplate($template);
            $mail->setVars($variables);
            $mail = $this->ressourceService->save($mail);
            return $this->success($mail);
        }
        return $this->unauthorized($this->getUserID());
    }
}
