<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\JSONResponse;
use \OCP\IRequest;
use \OCP\IUserSession;
use \OCP\Constants;

use OCA\ISDN\Db\CheckIn;
use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\CheckInService;
use OCA\ISDN\Service\PersonService;
use OCA\ISDN\Service\EventService;

/**
 * @extends ARessourceController<\OCA\ISDN\Db\CheckIn,\OCA\ISDN\Db\CheckInMapper, CheckInService>
 */
class CheckInController extends ARessourceController
{
    /** @var string */
    protected $permissionName = "CheckIns";
    /** @var PersonService */
    protected $personService;
    /** @var EventService */
    protected $eventService;

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        CheckInService $service,
        EventService $eventService,
        PersonService $personService
    ) {
        parent::__construct($request, $userSession, $roleService, $service);
        $this->eventService = $eventService;
        $this->personService = $personService;
    }

    /**
     * @NoAdminRequired
     * @PublicPage
     * @NoCSRFRequired
     */
    public function create(int $person, int $event, string $time): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_CREATE)) {
            $event = $this->eventService->findById($event);
            $person = $this->personService->findById($person);
            $checkIn = new CheckIn();
            $checkIn->setPerson($person);
            $checkIn->setEvent($event);
            $checkIn->setCheckIn(new \DateTime($time));
            $checkIn = $this->ressourceService->save($checkIn);
            return $this->success($checkIn);
        }
        return $this->unauthorized($this->getUserID());
    }
}
