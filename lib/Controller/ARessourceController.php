<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\DataResponse;
use \OCP\IRequest;
use \OCP\IUserSession;
use \OCP\Constants;

use OCA\ISDN\Service\AService;
use OCA\ISDN\Service\RoleService;

/**
 *  @template         T of \OCA\ISDN\Db\EEntity
 *  @template         M of \OCA\ISDN\Db\EQBMapper<T>
 *  @template         S of \OCA\ISDN\Service\AService
 */
abstract class ARessourceController extends AController
{
    /** @var S */
    protected $ressourceService;
    /** @var string */
    protected $permissionName;

    /**
     * @param S $ressourceService
     */
    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        AService $ressourceService
    ) {
        parent::__construct($request, $userSession, $roleService);
        $this->ressourceService = $ressourceService;
    }

    protected function hasPermission(int $level) : bool
    {
        return $this->roleService->hasPermission($this->getUser(), $this->permissionName, $level);
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function index(?int $limit = 20, ?int $offset = null): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_READ)) {
            return $this->success($this->ressourceService->findAll(true, $limit, $offset));
        }
        return $this->unauthorized($this->getUserID());
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function show(int $id): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_READ)) {
            return $this->success($this->ressourceService->findById($id, true));
        }
        return $this->unauthorized($this->getUserID());
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function destroy(int $id): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_DELETE)) {
            $entity = $this->ressourceService->markAsDeleted($id);
            return $this->success($entity);
        }
        return $this->unauthorized($this->getUserID());
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function restore(int $id, ?int $deletedAt): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_DELETE)) {
            $entity = $this->ressourceService->removeDeletionMark($id);
            return $this->success($entity);
        }
        return $this->unauthorized($this->getUserID());
    }
}
