<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\JSONResponse;
use \OCP\IRequest;
use \OCP\IUserSession;
use \OCP\Constants;

use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\ScheduledEventService;
use OCA\ISDN\Db\ScheduledEvent;

/**
* @extends ARessourceController<\OCA\ISDN\Db\ScheduledEvent,\OCA\ISDN\Db\ScheduledEventMapper,ScheduledEventService>
 */
class ScheduledEventController extends ARessourceController
{
    /** @var ScheduledEventService */
    protected $scheduledEventService;
    /** @var string */
    protected $permissionName = "ScheduledEvents";

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        ScheduledEventService $scheduledEventService
    ) {
        parent::__construct($request, $userSession, $roleService, $scheduledEventService);
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function create(string $name, string $duration, string $schedule, string $address): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_CREATE)) {
            $scheduledEvent = new ScheduledEvent();
            $scheduledEvent->setName($name);
            $scheduledEvent->setDuration($duration);
            $scheduledEvent->setSchedule($schedule);
            $scheduledEvent->setAddress($address);
            $scheduledEvent = $this->ressourceService->save($scheduledEvent);
            return $this->success($scheduledEvent);
        }
        return $this->unauthorized($this->getUserID());
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function update(int $id, string $name, string $duration, string $schedule, string $address): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_UPDATE)) {
            $scheduledEvent = $this->ressourceService->findById($id, true);
            $scheduledEvent->setName($name);
            $scheduledEvent->setDuration($duration);
            $scheduledEvent->setSchedule($schedule);
            $scheduledEvent->setAddress($address);
            $scheduledEvent = $this->ressourceService->save($scheduledEvent);
            return $this->success($scheduledEvent);
        }
        return $this->unauthorized($this->getUserID());
    }
}
