<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use \OCP\IRequest;
use \OCP\IUserSession;

use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\PersonService;

/**
* @extends ARessourceController<\OCA\ISDN\Db\Person,\OCA\ISDN\Db\PersonMapper, PersonService>
 */
class PersonController extends ARessourceController
{
    /** @var PersonService */
    protected $personService;
    /** @var string */
    protected $permissionName = "Persons";

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        PersonService $personService
    ) {
        parent::__construct($request, $userSession, $roleService, $personService);
    }
}
