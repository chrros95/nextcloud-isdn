<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use \OCP\IRequest;
use \OCP\IUserSession;

use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\LinkService;

/**
 * @extends ARessourceController<\OCA\ISDN\Db\Link,\OCA\ISDN\Db\LinkMapper,LinkService>
 */
class LinkController extends ARessourceController
{
    /** @var LinkService */
    protected $linkService;
    /** @var string */
    protected $permissionName = "Links";

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        LinkService $linkService
    ) {
        parent::__construct($request, $userSession, $roleService, $linkService);
    }
}
