<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\DataResponse;
use \OCP\IRequest;
use \OCP\IUserSession;
use \OCP\Constants;

use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\EventService;
use OCA\ISDN\Service\PersonService;
use OCA\ISDN\Db\Event;

/**
 * @extends ARessourceController<\OCA\ISDN\Db\Event,\OCA\ISDN\Db\EventMapper,EventService>
 */
class EventController extends ARessourceController
{
    /** @var string */
    protected $permissionName = "Events";

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        EventService $eventService
    ) {
        parent::__construct($request, $userSession, $roleService, $eventService);
    }

    /**
     * @NoAdminRequired
     * @PublicPage
     * @NoCSRFRequired
     */
    public function index(?int $limit = 20, ?int $offset = null, ?string $filter = null): JSONResponse
    {
        if ($filter == 'now') {
            return $this->success($this->ressourceService->findCurrentEvents());
        }
        if ($this->hasPermission(Constants::PERMISSION_READ)) {
            return $this->success($this->ressourceService->findAll(true, $limit, $offset));
        }
        return $this->unauthorized($this->getUserID());
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function create(string $name, string $start, string $end, string $address): JSONResponse
    {
        if ($this->hasPermission(Constants::PERMISSION_CREATE)) {
            $event = new Event();
            $event->setName($name);
            $event->setStart(new \DateTime($start));
            $event->setEnd(new \DateTime($end));
            $event->setAddress($address);
            $event = $this->ressourceService->save($event);
            return $this->success($event);
        }
        return $this->unauthorized($this->getUserID());
    }
}
