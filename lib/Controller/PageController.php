<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use \OCP\IRequest;
use \OCP\IUserSession;

use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Service\LinkService;

class PageController extends AController
{
    /** @var LinkService */
    protected $linkService;

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService,
        LinkService $linkService
    ) {
        parent::__construct($request, $userSession, $roleService);
        $this->linkService = $linkService;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function index(): TemplateResponse
    {
        if ($this->roleService->hasAnyPermission($this->getUser())) {
            return new TemplateResponse($this->appName, 'index', [
                "appName" => $this->appName
            ]);
        }
        return new TemplateResponse($this->appName, 'unauthorized', [
            "links" => $this->linkService->findAll()
        ]);
    }


    /**
     * @NoAdminRequired
     * @PublicPage
     * @NoCSRFRequired
     */
    public function checkIn(): TemplateResponse
    {
        return new TemplateResponse($this->appName, 'checkIn', [
            "appName" => $this->appName
        ]);
    }
}
