<?php
namespace OCA\ISDN\Controller;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;
use \OCP\IUser;
use \OCP\IRequest;
use \OCP\IUserSession;

use OCA\ISDN\AppInfo\Application;
use OCA\ISDN\Exception\NotLoggedInException;
use OCA\ISDN\Service\RoleService;
use OCA\ISDN\Utils\JSONResponseTrait;

abstract class AController extends Controller
{
    use JSONResponseTrait;

    /** @var IUserSession|null */
    protected $userSession;
    /** @var RoleService */
    protected $roleService;

    public function __construct(
        IRequest $request,
        ?IUserSession $userSession,
        RoleService $roleService
    ) {
        parent::__construct(Application::ID, $request);
        $this->userSession = $userSession;
        $this->roleService = $roleService;
    }

    protected function getUser() : ?IUser
    {
        if ($this->userSession === null) {
            throw new NotLoggedInException();
        }
        return $this->userSession->getUser();
    }

    protected function getUserID() : ?string
    {
        $user = $this->getUser();
        if (!is_null($user)) {
            return $user->getUID();
        }
        return null;
    }
}
