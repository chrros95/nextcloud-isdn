<?php
namespace OCA\ISDN\Utils;

class JSONDateTime extends \DateTime implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return $this->format(static::ISO8601);
    }
}
