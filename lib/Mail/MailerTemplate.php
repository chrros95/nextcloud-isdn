<?php
namespace OCA\ISDN\Mail;

use OCP\Mail\IEMailTemplate;

class MailerTemplate implements IEMailTemplate
{
    /** @var array<string> */
    protected $from = [];
    /** @var string */
    protected $subject;
    /** @var string */
    protected $text;
    /** @var string */
    protected $html;
    /** @var array<mixed> */
    protected $vars = [];

    /** @param array<string> $from */
    public function setFrom(array $from) : void
    {
        $this->from = $from;
    }

    /** @return array<string> $from */
    public function getFrom() : array
    {
        return $this->from;
    }

    public function setSubject(string $subject) : void
    {
        $this->subject = $subject;
    }

    public function setText(string $text) : void
    {
        $this->text = $text;
    }

    public function setHTML(string $html) : void
    {
        $this->html = $html;
    }

    /**
     * @param array<mixed> $vars
     */
    public function setVars(array $vars) : void
    {
        $this->vars = $vars;
    }

    public function addHeader():void
    {
    }

    public function addHeading(string $title, $plainTitle = ''):void
    {
    }

    public function addBodyText(string $text, $plainText = ''):void
    {
    }

    /**
     * @param int $plainIndent
     */
    public function addBodyListItem(
        string $text,
        string $metaInfo = '',
        string $icon = '',
        $plainText = '',
        $plainMetaInfo = '',
        $plainIndent = 0
    ):void {
    }

    public function addBodyButtonGroup(
        string $textLeft,
        string $urlLeft,
        string $textRight,
        string $urlRight,
        string $plainTextLeft = '',
        string $plainTextRight = ''
    ):void {
    }

    public function addBodyButton(string $text, string $url, $plainText = ''):void
    {
    }

    public function addFooter(string $text = '', ?string $lang = null):void
    {
    }

    public function renderSubject(): string
    {
        return $this->replaceVars($this->subject);
    }

    public function renderHtml(): string
    {
        return $this->replaceVars($this->html);
    }

    public function renderText() : string
    {
        return $this->replaceVars($this->text);
    }

    private function replaceVars(string $s):string
    {
        foreach ($this->vars as $key => $value) {
            $tmp = preg_replace("/{{\s*$key\s*}}/", $value, $s);
            if (!is_null($tmp)) {
                $s = $tmp;
            }
        }
        return $s;
    }
}
