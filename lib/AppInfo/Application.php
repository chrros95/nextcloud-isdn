<?php
namespace OCA\ISDN\AppInfo;

use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\IServerContainer;
use OCA\ISDN\Service\PermissionService;
use OCA\ISDN\Capabilities;

class Application extends App implements IBootstrap
{

    const ID = "isdn";

    public function __construct()
    {
        parent::__construct(self::ID);
    }

    public function register(IRegistrationContext $context) : void
    {
        // Register Capabilities
        include_once __DIR__ . '/../../vendor/autoload.php';
    }

    public function boot(IBootContext $context): void
    {
        $this->registerNavigationEntries($context->getServerContainer());
    }

    /**
     * @param IServerContainer $server
     */
    public function registerNavigationEntries(IServerContainer $server): void
    {
    }
}
