import { gettext, getBaseUrl } from 'nextcloud-react'
import {
  EventView,
  PersonView,
  LinkView,
  ScheduledEventView
} from 'main/views'

export const config = {
  appID: 'isdn',
  routes: [{
    path: '/events',
    name: gettext('Events'),
    icon: 'calendar-dark',
    component: EventView,
    children: [{
      subject: 'Events',
      path: '/:id?',
      component: EventView,
      hide_in_nav: true
    }, {
      subject: 'ScheduledEvents',
      path: '/scheduled/:id?',
      name: gettext('Scheduled Events'),
      component: ScheduledEventView
    }, {
      path: getBaseUrl('isdn') + '/checkin',
      name: gettext('Open external portal'),
      icon: 'external',
      absolute: true
    }]
  },
  {
    subject: 'Persons',
    path: '/persons/:id?',
    name: gettext('Personen'),
    icon: 'group',
    component: PersonView
  },
  {
    subject: 'Links',
    path: '/links',
    name: gettext('Links'),
    icon: 'toggle-filelist',
    component: LinkView
  }]
}
