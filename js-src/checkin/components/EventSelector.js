import React from 'react'
import { AppInput, useData, getDateRage } from 'nextcloud-react'

export default function EventSelector (props) {
  const events = useData('events')
  return (
    <AppInput tag='select' options={events ? events.map((e, i) => [e.id, e.name+' ('+getDateRage(e.start,e.end)+')']) : []} component='selectedevent' isView={false} />
  )
}
