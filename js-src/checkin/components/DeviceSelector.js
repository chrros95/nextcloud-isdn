import React from 'react'
import { AppInput, useData } from 'nextcloud-react'

export default function DeviceSelector (props) {
  const devices = useData('devicelist')
  return (
    <AppInput tag='select' options={devices ? devices.map((d, i) => [d.deviceId, d.label]) : []} component='deviceselection' isView={false} />
  )
}
