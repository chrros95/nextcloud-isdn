import { all, spawn, call } from 'redux-saga/effects'
import { sagas as appSagas } from 'nextcloud-react'
import { sagas as checkinSagas } from './checkinSagas'

export default function * sagas () {
  const sagas = [
    ...appSagas,
    ...checkinSagas
  ]
  yield all(sagas.map(saga =>
    spawn(function * () {
      while (true) {
        try {
          yield call(saga)
          break
        } catch (e) {
          console.log(e)
        }
      }
    }))
  )
}
