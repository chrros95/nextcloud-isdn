import { takeLatest, takeEvery, put, select } from 'redux-saga/effects'
import 'url-search-params-polyfill'
import { API, reducerConstants, gettext, showErrorToast } from 'nextcloud-react'
import { BrowserCodeReader } from '@zxing/browser'
import { Base64 } from 'js-base64'
import { config } from 'config'
import { rConstants as checkinConstants } from 'checkin/reducers/checkinReducer'

function * requestPermissions () {
  if ('mediaDevices' in navigator && navigator.mediaDevices) {
    try {
      yield navigator.mediaDevices.getUserMedia({ video: {
          width: { ideal: 4096 },
          height: { ideal: 2160 }
      }})
      return true
    } catch (e) {
      console.log('Webcam error', e)
    }
  }
  return false
}

function * loadDevices () {
  const hasPermission = yield requestPermissions()
  if (hasPermission) {
    let selectedDevice = select((state) => state.app.components.deviceselection)

    yield put({
      type: reducerConstants.loading.f.ADD_STEP,
      payload: {
        key: 'devices',
        state: 'loading',
        message: gettext('Loading Camera Devices'),
        counter: 0,
        type: 'fullscreen'
      }
    })
    const videoInputDevices = yield BrowserCodeReader.listVideoInputDevices()
    yield put({
      type: reducerConstants.app.f.SET_COMPONENT_STATE,
      payload: { component: 'devicelist', data: videoInputDevices }
    })
    if (Array.isArray(videoInputDevices) && videoInputDevices.length > 0) {
      let isDeviceSelected = false
      if (selectedDevice) {
        for (const device of videoInputDevices) {
          if (device.deviceId === selectedDevice) {
            isDeviceSelected = true
          }
        }
      }
      if (!isDeviceSelected) {
        selectedDevice = videoInputDevices[0].deviceId
        yield put({
          type: reducerConstants.app.f.SET_COMPONENT_STATE,
          payload: { component: 'deviceselection', state: selectedDevice }
        })
      }
      yield put({
        type: reducerConstants.app.f.SET_COMPONENT_STATE,
        payload: { component: 'video', state: 'loaded' }
      })
    }
    yield put({
      type: reducerConstants.loading.f.REMOVE_STEP,
      payload: { key: 'devices' }
    })
  } else {
    console.log('Denied Permisssion')
    yield put({
      type: reducerConstants.loading.f.ADD_STEP,
      payload: {
        key: 'devices',
        state: 'failed',
        message: gettext('Please, grant permission to access the camera'),
        counter: 0,
        type: 'fullscreen'
      }
    })
  }
}

function * loadEvents () {
  const response = yield API.get(config.appID, 'Events', undefined, { filter: 'now' })
  const entities = yield response.json()
  yield put({
    type: reducerConstants.app.f.SET_COMPONENT_STATE,
    payload: { component: 'events', data: entities.data }
  })
  if (Array.isArray(entities.data) && entities.data.length > 0) {
    yield put({
      type: reducerConstants.app.f.SET_COMPONENT_STATE,
      payload: { component: 'selectedevent', state: entities.data[0].id, isView: false }
    })
  }
}

function * handleAction () {
  const action = yield select((state) => state.app.action)
  if (action.action === 'scan_result') {
    const eventId = yield select((state) => state.app.components.selectedevent)
    const data = JSON.parse(Base64.decode(action.payload.text))
    const scanAt = (new Date().getTime()) / 1000
    const lastScan = yield select((state) => state.checkin.persons[data.id] || 0)
    const delta = scanAt - lastScan
    console.log('Got payload', lastScan, scanAt, data)
    if (delta > 30) {
      yield put({
        type: checkinConstants.f.ADD,
        payload: {
          id: data.id,
          time: scanAt,
          event: eventId
        }
      })
      try {
        yield API.post(config.appID, 'CheckIns', {
          person: data.id,
          time: new Date(scanAt * 1000).toISOString(),
          event: eventId
        })
        yield put({
          type: reducerConstants.app.f.SET_COMPONENT_STATE,
          payload: {
            component: 'modal',
            state: {
              data: gettext('Hallo ' + data.forename + ', der Scan wurde registriert.'),
              timeout: 2
            }
          }
        })
      } catch (e) {
        console.error('Failed to fetch', e)
        yield showErrorToast(gettext('Ups, an error occured.'))
      }
    } else if (delta > 5) {
      yield put({
        type: reducerConstants.app.f.SET_COMPONENT_STATE,
        payload: {
          component: 'modal',
          state: {
            data: gettext('Hallo ' + data.forename + ', bitte warte ' + Math.round(30 - delta) + ' Sekunden vor dem nächsten Scan .'),
            timeout: 4
          }
        }
      })
    }
  }
}

function * watchForInit () {
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, loadDevices)
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, loadEvents)
}

function * waitForAction () {
  yield takeEvery(reducerConstants.app.f.ACTION_REQUESTED, handleAction)
}

export const sagas = [
  watchForInit,
  waitForAction
]
