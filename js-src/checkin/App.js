import React from 'react'
import { ToastContainer, LoadingSpinner, AppModal, useComponentState } from 'nextcloud-react'
import { CheckInView } from 'checkin/views'

import './App.css'

export default function AppProivder (props) {
  const [modal] = useComponentState('modal')
  return (
    <>{modal ? <AppModal {...modal} /> : <></>}
      <ToastContainer />
      <LoadingSpinner type='fullscreen' />
      <CheckInView />
    </>
  )
}
