import React from 'react'
import { BrowserAztecCodeReader } from '@zxing/browser'

import {
  useViewState, useComponentState, useActionDispatcher, useData,
  gettext
} from 'nextcloud-react'

import { DeviceSelector, EventSelector } from 'checkin/components'
import './CheckInView.css'

export default function CheckInView (props) {
  useViewState('CheckInView')
  const [selectedDevice] = useComponentState('deviceselection')
  const dispatch = useActionDispatcher()
  const events = useData('events')
  const ref = React.createRef()
  React.useEffect(() => {
    const codeReader = new BrowserAztecCodeReader()
    codeReader.decodeFromVideoDevice(selectedDevice, ref.current, (result, error, controls) => {
      if (result) {
        try {
          dispatch(undefined, 'scan_result', result)
        } catch (e) {
          console.error('Scan Error', e)
        }
      }
    })
  }, [selectedDevice, (!events || !Array.isArray(events) || events.length < 1)])
  if (!events || !Array.isArray(events) || events.length < 1) {
    return (
      <>
        {gettext('Currently, now events are known.')}
      </>
    )
  }
  return (
    <>
      <video className='checkin-video' ref={ref} />
      <div className='overlay'>
        <div className='options bottom-left'>
          <EventSelector />
        </div>
        <div className='options bottom-right'>
          <DeviceSelector />
        </div>
      </div>
    </>
  )
}
