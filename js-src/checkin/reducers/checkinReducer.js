import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

export const rConstants = {
  f: {
    ADD: 'checkin/add',
    SYNC_STARTED: 'checkin/sync',
    SYNC_DONE: 'checkin/sync_done'
  },
  c: {

  }
}

const initialState = {
  persons: {
  },
  state: undefined,
  lastCheckIn: undefined
}

const persistConfig = {
  key: 'checkin',
  storage: storage,
  blacklist: ['state'],
  stateReconciler: autoMergeLevel2,
  debug: true
}

const internalAppReducer = (state = initialState, action) => {
  switch (action.type) {
    case rConstants.f.ADD: {
      state.persons[action.payload.id] = action.payload.time
      return {
        ...state,
        persons: {
          ...state.persons
        },
        lastCheckIn: action.payload
      }
    }
    case rConstants.f.SYNC_STARTED: {
      return {
        ...state,
        state: 'started'
      }
    }
    case rConstants.f.SYNC_DONE: {
      return {
        ...state,
        state: action.payload
      }
    }
    default:
      return state
  }
}

export default persistReducer(persistConfig, internalAppReducer)
