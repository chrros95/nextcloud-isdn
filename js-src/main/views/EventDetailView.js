import React from 'react'
import {
  useActionDispatcher, gettext,
  AppTable, AppTableRow, AppTableCell, AppInput
} from 'nextcloud-react'

import { CheckInRow } from 'main/components'

export default function EventDetailView (props) {
  const size = 6
  const actionDispatcher = useActionDispatcher()
  const item = props.item
  let checkIns = undefined;
  if(Array.isArray(item.checkIns)){
      checkIns = {}
      item.checkIns.map((e) =>{
          if(!(e.person.id in checkIns)){
              checkIns[e.person.id] = []
          }
          checkIns[e.person.id].push(e)
      })
      let arr = []
      for(const [p, c] of Object.entries(checkIns)){
          for(let i=0; i<c.length; i = i+2){
              console.log(i,c[i], c[i+1])
             arr.push({
                 person: c[i].person,
                 checkIn: c[i].checkIn,
                 checkOut: ((i+1) in c ? c[i+1].checkIn : undefined)
             })
          }
      }
      checkIns = arr
  }
  return (
    <>
      <h2>{item.name}</h2>
      <AppTable>
        <AppTableRow>
          <AppTableCell>{gettext('Titel')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='name' value={item.name} required /></AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Start')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='start' type='datetime-local' value={item.start} />
          </AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('End')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='end' type='datetime-local' value={item.end} />
          </AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Location')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='address' value={item.address} />
          </AppTableCell>
        </AppTableRow>
      </AppTable>
      <button onClick={(e) => actionDispatcher(e, 'save_event', item)}>{gettext('Speichern')}</button>

      {item.id
        ? (
          <><h2>{gettext('Check-Ins')}</h2> {checkIns
            ? (
              <AppTable
                rowComponent={CheckInRow}
                columns={[gettext('Person'), gettext('Check-In/Check-Out'), '']}
                data={checkIns}
              >
                {gettext('No check-ins have been found, yet')}
              </AppTable>
              )
            : <></>}
          </>
          )
        : <></>}

    </>
  )
}
