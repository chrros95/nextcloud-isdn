import React from 'react'
import {
  gettext,
  MainContent,
  AppContent,
  AppTable,
  useViewState, useData, useLoadingState, getBaseUrl
} from 'nextcloud-react'
import { LinkRow } from 'main/components'
import { config } from 'config'

export default function LinkView (props) {
  useViewState('LinkView')
  const links = useData('LinkView')
  const isLoading = useLoadingState('LinkView')
  console.log('Base', getBaseUrl(config.appID))
  return (
    <MainContent url={getBaseUrl(config.appID)}>
      <AppContent>
        <AppTable isLoading={isLoading === 'loading'} columns={[gettext('Service'), gettext('Link')]} data={links} rowComponent={LinkRow}>
          {gettext('We found no Links, yet')}
        </AppTable>
      </AppContent>
    </MainContent>
  )
}
