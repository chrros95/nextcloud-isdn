import React from 'react'
import {
  MainContent,
  AppContent,
  AppContentList,
  AppContentDetails,
  useViewState, useData, useLoadingState, gettext, getBaseUrl
} from 'nextcloud-react'

import { ScheduledEventListItem } from 'main/components'
import { ScheduledEventDetailView } from 'main/views'
import { config } from 'config'

export default function ScheduledEventView (props) {
  useViewState('ScheduledEventView')
  const isLoading = useLoadingState('ScheduledEventView')
  const scheduledEvents = useData('ScheduledEventView')

  return (
    <MainContent url={getBaseUrl(config.appID)}>
      <AppContent>
        <AppContentList data={scheduledEvents} sort='name' isLoading={isLoading === 'loading'} routeSubject='ScheduledEvents' component={ScheduledEventListItem}>{gettext('We found no scheduled events, yet')}</AppContentList>
        <AppContentDetails component={ScheduledEventDetailView} routeSubject='ScheduledEvents' appID={config.appID} routes={config.routes}>{gettext('Please select a scheduled Event')}</AppContentDetails>
      </AppContent>
    </MainContent>
  )
}
