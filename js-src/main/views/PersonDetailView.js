import React from 'react'
import { useActionDispatcher, gettext, AppTable, AppTableRow, AppTableCell } from 'nextcloud-react'
import { refrenceTypeToLabel } from 'main/utils'

export default function PersonDetailView (props) {
  const size = 6
  const actionDispatcher = useActionDispatcher()
  const contactDetails = []; const references = []
  if (props.item && 'contacts' in props.item && props.item.contacts instanceof Object) {
    for (const [label, value] of Object.entries(props.item.contacts)) {
      contactDetails.push(
        <AppTableRow key={label}>
          <AppTableCell>{gettext(label)}</AppTableCell>
          <AppTableCell size={size}>{value.data}</AppTableCell>
        </AppTableRow>
      )
    }
  }
  if (props.item && 'references' in props.item && props.item.references instanceof Object) {
    for (const [type, value] of Object.entries(props.item.references)) {
      references.push(
        <AppTableRow key={type}>
          <AppTableCell>{gettext(refrenceTypeToLabel(type))}</AppTableCell>
          <AppTableCell size={size}>{value}</AppTableCell>
        </AppTableRow>
      )
    }
  }
  return (
    <>
      <h2>{props.item.forename} {props.item.surename}</h2>
      <AppTable>
        <AppTableRow>
          <AppTableCell>{gettext('Forename')}</AppTableCell>
          <AppTableCell size={size}>{props.item.forename}</AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell>{gettext('Surename')}</AppTableCell>
          <AppTableCell size={size}>{props.item.surename}</AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell>{gettext('Birthday')}</AppTableCell>
          <AppTableCell size={size}>{props.item.birthday}</AppTableCell>
        </AppTableRow>
      </AppTable>
      <h2>{gettext('Contact Details')}</h2>
      <AppTable>{contactDetails}</AppTable>
      <h2>{gettext('References')}</h2>
      <AppTable>{references}</AppTable>
      <button onClick={(e) => actionDispatcher(e, 'generate_checkin_code', props.item)}>{gettext('Generate Check-In-Code')}</button>
    </>
  )
}
