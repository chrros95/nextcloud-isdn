import React from 'react'
import {
  MainContent,
  AppContent,
  AppContentList,
  AppContentDetails,
  useViewState, useData, useLoadingState, gettext, getBaseUrl
} from 'nextcloud-react'
import { EventDetailView } from 'main/views'
import { EventListItem } from 'main/components'
import { config } from 'config'

export default function EventView (props) {
  useViewState('EventView')
  const isLoading = useLoadingState('EventView')
  const scheduledEvents = useData('EventView')

  return (
    <MainContent url={getBaseUrl(config.appID)}>
      <AppContent>
        <AppContentList data={scheduledEvents} sort='start' isLoading={isLoading} routeSubject='Events' component={EventListItem}>{gettext('We found no events, yet')}</AppContentList>
        <AppContentDetails component={EventDetailView} routeSubject='Events' appID={config.appID} routes={config.routes}>{gettext('Please select a Event')}</AppContentDetails>
      </AppContent>
    </MainContent>
  )
}
