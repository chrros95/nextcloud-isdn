import React from 'react'
import { useActionDispatcher, gettext, AppTable, AppTableRow, AppTableCell, AppInput } from 'nextcloud-react'
import { EventRow } from 'main/components'
import { composeRRule, parseSchedule } from 'main/utils'
import { RRule, Weekday } from 'rrule'

export default function ScheduledEventDetailView (props) {
  const size = 6
  const actionDispatcher = useActionDispatcher()
  let item = props.item; let s
  if (!('freq' in props.item) || !props.item.freq) {
    item = parseSchedule(props.item)
  }
  const rrule = item.freq ? composeRRule(item) : undefined
  const options = [0, 1, 2, 3, 4, 5, 6].map((r, i) => {
    s = (new Weekday(r)).toString()
    return [s, gettext('weekday_' + s), (item.byweekday && s in item.byweekday && item.byweekday[s])]
  })
  return (
    <>
      <h2>{item.name}</h2>
      <AppTable>
        <AppTableRow>
          <AppTableCell>{gettext('Titel')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='name' value={item.name} required /></AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Duration')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='duration' type='time' value={item.duration} />
          </AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Location')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='address' value={item.address} />
          </AppTableCell>
        </AppTableRow>
      </AppTable>
      <h2>{gettext('Schedule')}</h2>
      <AppTable>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Start date')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' id='start' type='datetime-local' value={item.start} />
          </AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Frequency')}</AppTableCell>
          <AppTableCell size={size}><AppInput
            component='detailsItem' id='freq' tag='select' value={item.freq} options={RRule.FREQUENCIES.map((r, i) => {
              return [RRule[r], gettext(r)]
            })}
                                    />
          </AppTableCell>
        </AppTableRow>
        {
            parseInt(item.freq) === RRule.WEEKLY
              ? (
                <AppTableRow>
                  <AppTableCell className='label'>{gettext('By Weekday')}</AppTableCell>
                  <AppTableCell size={size}><AppInput component='detailsItem' id='byweekday' type='checkbox' value={item.byweekday} options={options} /></AppTableCell>
                </AppTableRow>
                )
              : <></>
        }
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Interval')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' type='number' id='interval' validationMsg={gettext('This must be an int')} value={item.interval} /></AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('Count')}</AppTableCell>
          <AppTableCell size={size}><AppInput component='detailsItem' type='number' id='count' validationMsg={gettext('This must be an int')} value={item.count} /><span className='input-hint'>{gettext('0 = endless')}</span></AppTableCell>
        </AppTableRow>
        <AppTableRow>
          <AppTableCell className='label'>{gettext('In Words')}</AppTableCell>
          <AppTableCell size={size}>{rrule ? rrule.toText() : ''}</AppTableCell>
        </AppTableRow>
      </AppTable>
      <button onClick={(e) => actionDispatcher(e, 'save_scheduledevent', item)}>{gettext('Speichern')}</button>

      {item.id
        ? (
          <><h2>{gettext('Events')}</h2> {item.events
            ? (
              <AppTable
                rowComponent={EventRow}
                columns={[gettext('ID'), gettext('Title'), gettext('Start/Ende'), '']}
                data={item.events}
              >
                {gettext('No events have been calculated, yet')}
              </AppTable>
              )
            : <></>}
          </>
          )
        : <></>}

    </>
  )
}
