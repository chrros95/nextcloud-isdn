import React from 'react'
import {
  MainContent,
  AppContent,
  AppContentList,
  AppContentDetails,
  useViewState, useData, useLoadingState, getBaseUrl
  , gettext
} from 'nextcloud-react'

import { PersonListItem } from 'main/components'
import { PersonDetailView } from 'main/views'
import { config } from 'config'

export default function PersonView (props) {
  useViewState('PersonView')
  const isLoading = useLoadingState('PersonView')
  const persons = useData('PersonView')
  if (persons) {
    persons.sort((a, b) => {
      if (a.name && b.name) {
        return a.name.localeCompare(b.name, undefined, { numeric: true, sensitivity: 'base' })
      }
      return 0
    })
  }

  return (
    <MainContent url={getBaseUrl(config.appID)}>
      <AppContent>
        <AppContentList data={persons} isLoading={isLoading === 'loading'} component={PersonListItem}>{gettext('We found no Persons, yet')}</AppContentList>
        <AppContentDetails component={PersonDetailView} routeSubject='Persons' appID={config.appID} routes={config.routes}>{gettext('Please select a person')}</AppContentDetails>
      </AppContent>
    </MainContent>
  )
}
