import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { LoadingSpinner, AppModal, routeToFullURI, useComponentState } from 'nextcloud-react'
import { config } from 'config'

export default function AppProivder (props) {
  const [modal] = useComponentState('modal')
  return (
    <>{modal ? <AppModal {...modal} /> : <></>}
      <LoadingSpinner type='fullscreen' />
      <ConnectedRouter history={props.history}>
        <Switch>
          {config.routes.map((route, key) => {
            const composedRoutes = [<Route
              path={routeToFullURI(route.path, undefined, config.appID)}
              component={route.component}
              key={key}
              exact={route.exact}
                                    />]
            if ('children' in route) {
              route.children.forEach((r, k) =>
                composedRoutes.push(
                  <Route
                    path={routeToFullURI((!r.absolute ? route.path + r.path : r.path), undefined, config.appID)}
                    component={r.component}
                    key={key + '_' + k}
                    exact={r.exact}
                  />
                )
              )
            }
            return composedRoutes.reverse()
          })}
          <Redirect to={routeToFullURI('/links', undefined, config.appID)} />
        </Switch>
      </ConnectedRouter>
    </>
  )
}
