export function refrenceTypeToLabel (type) {
  switch (type) {
    case 'MV':
      return 'DLRG Nummernkreis'
    case 'SE':
      return 'DLRG-Manager ID'
    case 'ISC':
      return 'DLRG-ISC'
    case 'NC':
      return 'Nextcloud'
    default:
      return type
  }
}
