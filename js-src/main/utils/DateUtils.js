import { RRule, Weekday } from 'rrule'
import { getInputDate } from 'nextcloud-react'

export function composeRRule (item) {
  if (item.start) {
    const rruleOptions = {
      freq: item.freq,
      dtstart: new Date(item.start),
      interval: item.interval,
      byweekday: item.byweekday ? Object.keys(item.byweekday).map((w, i) => Weekday.fromStr(w)) : []
    }
    if (item.count && item.count > 0) {
      rruleOptions.count = item.count
    }
    return new RRule(rruleOptions)
  }
}

export function parseSchedule (event) {
  if (event.schedule) {
    const rrule = RRule.fromString(event.schedule)
    console.log(rrule, rrule.options)
    event.freq = rrule.options.freq
    event.interval = rrule.options.interval
    event.count = Number.isInteger(rrule.options.count) ? rrule.options.count : 0
    event.start = getInputDate(rrule.options.dtstart)
    event.byweekday = {}
    for (const d of rrule.options.byweekday) {
      event.byweekday[(new Weekday(d)).toString()] = true
    }
  }
  return event
}
