import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import { PersistGate } from 'redux-persist/integration/react'
import configureStore, { history } from 'store'
import { config } from 'config'
import AppProivder from 'main/App'
import sagas from './sagas'

const { store, persistor } = configureStore(sagas)
store.dispatch({
  type: 'set_routes',
  payload: { routes: config.routes }
})

window.appID = config.appID

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppProivder history={history} />
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById('content')
)
