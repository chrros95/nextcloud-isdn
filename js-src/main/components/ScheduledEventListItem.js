import React from 'react'
import { RRule } from 'rrule'
import { useActionDispatcher, AppContentListItem, AppContentListItemLineOne, AppContentListItemLineTwo, gettext } from 'nextcloud-react'

import { config } from 'config'

export default function ScheduledEventListItem (props) {
  const actionDispatcher = useActionDispatcher()
  const rule = RRule.fromString(props.item.schedule)
  const deleteAction = <div className='icon-delete' onClick={(e) => { e.stopPropagation(); actionDispatcher(e, 'delete_scheduledevent', props.item) }} />
  return (
    <AppContentListItem icon={props.item.name} item={props.item} details={deleteAction} routeSubject={props.routeSubject} appID={config.appID} routes={config.routes}>
      <AppContentListItemLineOne>{props.item.name}</AppContentListItemLineOne>
      <AppContentListItemLineTwo>{gettext('Schedule: ') + rule.toText(gettext)}</AppContentListItemLineTwo>
    </AppContentListItem>
  )
}
