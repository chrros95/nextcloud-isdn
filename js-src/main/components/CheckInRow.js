import React from 'react'
import { useRouteChanger, AppTableRow, AppTableCell, getDateRage } from 'nextcloud-react'
import { config } from 'config'

export default function CheckInRow (props) {
  const [routeChanger, url] = useRouteChanger('Checkins', props, config.appID, config.routes)
  console.log("Props", props)
  return (
    <AppTableRow>
      <AppTableCell>{props.person ? [props.person.surename, props.person.forename].join(', ') : ''}</AppTableCell>
      <AppTableCell>{getDateRage(props.checkIn, props.checkOut)}</AppTableCell>
      <AppTableCell><a href={url} onClick={routeChanger} className='icon-external' /></AppTableCell>
    </AppTableRow>
  )
}
