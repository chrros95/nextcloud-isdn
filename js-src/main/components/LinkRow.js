import React from 'react'
import { AppTableRow, AppTableCell } from 'nextcloud-react'

export default function LinkRow (props) {
  return (
    <AppTableRow>
      <AppTableCell>{props.label}</AppTableCell>
      <AppTableCell><a href={props.href} target={props.target}>{props.href}</a></AppTableCell>
    </AppTableRow>
  )
}
