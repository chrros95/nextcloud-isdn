import React from 'react'
import { useActionDispatcher, getDateRage, AppContentListItem, AppContentListItemLineOne, AppContentListItemLineTwo } from 'nextcloud-react'

import { config } from 'config'

export default function EventListItem (props) {
  const actionDispatcher = useActionDispatcher()
  const deleteAction = <div className='icon-delete' onClick={(e) => { e.stopPropagation(); actionDispatcher(e, 'delete_event', props.item) }} />
  return (
    <AppContentListItem icon={props.item.name} item={props.item} details={deleteAction} routeSubject={props.routeSubject} appID={config.appID} routes={config.routes}>
      <AppContentListItemLineOne>{props.item.name}</AppContentListItemLineOne>
      <AppContentListItemLineTwo>{getDateRage(props.item.start, props.item.end)}</AppContentListItemLineTwo>
    </AppContentListItem>
  )
}
