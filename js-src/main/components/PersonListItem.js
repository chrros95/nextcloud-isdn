import React from 'react'
import { AppContentListItem, AppContentListItemLineOne, AppContentListItemLineTwo, gettext } from 'nextcloud-react'

import { config } from 'config'

export default function PersonListItem (props) {
  return (
    <AppContentListItem icon={props.item.surename} item={props.item} routeSubject='Persons' appID={config.appID} routes={config.routes}>
      <AppContentListItemLineOne>{props.item.surename}, {props.item.forename}</AppContentListItemLineOne>
      <AppContentListItemLineTwo>{gettext('Referenzen: ') + Object.values(props.item.references).join(', ')}</AppContentListItemLineTwo>
    </AppContentListItem>
  )
}
