import React from 'react'
import { useRouteChanger, AppTableRow, AppTableCell, getDateRage } from 'nextcloud-react'
import { config } from 'config'

export default function EventRow (props) {
  const [routeChanger, url] = useRouteChanger('Events', props, config.appID, config.routes)
  return (
    <AppTableRow>
      <AppTableCell>{props.name}</AppTableCell>
      <AppTableCell>{getDateRage(props.start, props.end)}</AppTableCell>
      <AppTableCell><a href={url} onClick={routeChanger} className='icon-external' /></AppTableCell>
    </AppTableRow>
  )
}
