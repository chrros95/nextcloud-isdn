import { takeLatest } from 'redux-saga/effects'
import 'url-search-params-polyfill'
import { reducerConstants, loadData } from 'nextcloud-react'

import { config } from 'config'

function * watchForLoadView () {
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, loadData, config.appID, 'LinkView', 'Links', false, config.routes, (e) => e)
}

export const sagas = [
  watchForLoadView
]
