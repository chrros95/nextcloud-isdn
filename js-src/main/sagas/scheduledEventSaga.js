import { takeLatest, takeEvery } from 'redux-saga/effects'
import {
  reducerConstants,
  loadData, activateNewButton, getInputDate,
  createEntity, deleteEntity, undoDeleteEntity, gettext, saveEntity
} from 'nextcloud-react'

import { composeRRule, parseSchedule } from 'main/utils'
import { RRule } from 'rrule'
import { config } from 'config'

function validator (entity) {
  console.log('Validator called', arguments)
  return ('name' in entity) && entity.name
}

function parser (entity, isToAPI = false) {
  if (isToAPI !== false) {
    const rrule = composeRRule(entity)
    return {
      name: entity.name,
      schedule: rrule.toString(),
      duration: entity.duration,
      address: entity.address
    }
  }
  return parseSchedule(entity)
}

function * watchForLoadView () {
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, loadData, config.appID, 'ScheduledEventView', 'ScheduledEvents', false, config.routes, parser)
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, activateNewButton, 'ScheduledEventView', gettext('Create a new scheduled event'))
}

function * watchForScrollAction () {
  yield takeEvery(reducerConstants.app.f.SCROLLED, loadData, config.appID, 'ScheduledEventView', 'ScheduledEvents', true, config.routes, parser)
}

function * watchForActionRequested () {
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, createEntity, 'ScheduledEventView', { name: '', freq: RRule.WEEKLY, interval: 1, count: 0, start: getInputDate(), duration: '01:00', address: '', byweekday: {} })
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, saveEntity, config.appID, 'ScheduledEventView', 'scheduledevent', 'ScheduledEvents', 'Scheduled event', validator, parser)
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, deleteEntity, config.appID, 'ScheduledEventView', 'ScheduledEvents', 'Scheduled event', 'scheduledevent')
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, undoDeleteEntity, config.appID, 'ScheduledEventView', 'scheduledevent', 'ScheduledEvents', 'Scheduled event')
}

export const sagas = [
  watchForLoadView,
  watchForScrollAction,
  watchForActionRequested
]
