import { call, put, takeLatest, select, takeEvery } from 'redux-saga/effects'
import 'url-search-params-polyfill'
import { reducerConstants, API, loadData, activateNewButton, gettext } from 'nextcloud-react'

import { azteccode } from 'bwip-js'
import { Base64 } from 'js-base64'
import { showSuccess, showError } from '@nextcloud/dialogs'
import { config } from 'config'

function parser(entity, isToAPI) {
    return entity
}

function * handleAction () {
  const action = yield select((state) => state.app.action)
  if (action.action === 'generate_checkin_code') {
    yield put({
      type: reducerConstants.app.f.SET_COMPONENT_STATE,
      payload: {
        component: 'modal',
        state: {
          img: gererateAztecCode({
            surename: action.payload.surename,
            forename: action.payload.forename,
            id: action.payload.id,
            references: action.payload.references
          }),
          inputs: {
            mail: {
              label: 'Send per E-Mail',
              type: 'email',
              value: (action.payload.contacts['E-Mail'] ? action.payload.contacts['E-Mail'].data : undefined)
            }
          },
          actions: [{
            label: 'send',
            close: true,
            id: 'person_send_code'
          }, {
            label: 'close',
            close: true,
            id: undefined
          }]
        },
        data: action.payload
      }
    })
  }
}

function * sendCode () {
  const modal = yield select((state) => state.app.components.modal)
  if (modal.last_action === 'person_send_code') {
    const response = yield call(API.post, 'Mails', {
      template: 'person_code',
      to: [modal.inputs.mail.value],
      variables: {
        img: modal.img,
        name: modal.data.forename
      }
    })
    if (response.status === 200) {
      showSuccess('Nachricht wurde an den Versand übergeben.')
    } else {
      console.error('Failed to send code', response, response.json())
      showError('Nachricht konnte nicht versendet werden.')
    }
  }
}

function gererateAztecCode (text) {
  try {
    const canvas = document.createElement('canvas')
    text = Base64.encode(JSON.stringify(text))
    azteccode(canvas, {
      bcid: 'azteccode',
      format: 'full',
      text: text,
      backgroundcolor: 'FFFFFF'
    })
    return canvas.toDataURL('image/png')
  } catch (e) {
    console.log('Failed to generate Aztec-Code', e)
    return undefined
  }
}

function * watchForLoadView () {
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, loadData, config.appID, 'PersonView', 'Persons', false, config.routes, parser)
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, activateNewButton, 'PersonView', gettext('Create a new person'))
}
function * watchForActions () {
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, handleAction)
}
function * watchForSendCodeAction () {
  yield takeLatest(reducerConstants.app.f.MODAL_ACTION_FIRED, sendCode)
}
function * watchForScrollAction () {
  yield takeEvery(reducerConstants.app.f.SCROLLED, loadData, config.appID, 'PersonView', 'Persons', true, config.routes, parser)
}

export const sagas = [
  watchForLoadView,
  watchForActions,
  watchForSendCodeAction,
  watchForScrollAction
]
