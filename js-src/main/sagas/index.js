import { all, spawn, call } from 'redux-saga/effects'
import { sagas as appSagas } from 'nextcloud-react'
import { sagas as linkSagas } from './linkSagas'
import { sagas as personSaga } from './personSaga'
import { sagas as scheduledEventSaga } from './scheduledEventSaga'
import { sagas as eventSaga } from './eventSaga'

export default function * sagas () {
  const sagas = [
    ...appSagas,
    ...linkSagas,
    ...personSaga,
    ...scheduledEventSaga,
    ...eventSaga
  ]
  yield all(sagas.map(saga =>
    spawn(function * () {
      while (true) {
        try {
          yield call(saga)
          break
        } catch (e) {
          console.log(e)
        }
      }
    }))
  )
}
