import { takeLatest, takeEvery } from 'redux-saga/effects'
import {
  reducerConstants,
  loadData, activateNewButton, getInputDate,
  createEntity, deleteEntity, undoDeleteEntity, gettext, saveEntity
} from 'nextcloud-react'
import { config } from 'config'

function validator (entity) {
  return true
}

function parser (entity, isToAPI = false) {
  const parsedEntity = entity
  if (!isToAPI) {
    parsedEntity.start = getInputDate(parsedEntity.start)
    parsedEntity.end = getInputDate(parsedEntity.end)
  } else {
    parsedEntity.start = new Date(parsedEntity.start).toISOString()
    parsedEntity.end = new Date(parsedEntity.end).toISOString()
  }
  return parsedEntity
}

function * watchForLoadView () {
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, loadData, config.appID, 'EventView', 'Events', false, config.routes, parser)
  yield takeLatest(reducerConstants.app.f.VIEW_LOADED, activateNewButton, 'EventView', gettext('Create a new event'))
}

function * watchForScrollAction () {
  yield takeEvery(reducerConstants.app.f.SCROLLED, loadData, config.appID, 'EventView', 'Events', true, config.routes, parser)
}

function * watchForActionRequested () {
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, createEntity, 'EventView', { name: '', start: getInputDate(), end: getInputDate(), address: '', checkins: [] })
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, saveEntity, config.appID, 'EventView', 'event', 'Events', 'Event', validator, parser)
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, deleteEntity, config.appID, 'EventView', 'Events', 'Event', 'event')
  yield takeLatest(reducerConstants.app.f.ACTION_REQUESTED, undoDeleteEntity, config.appID, 'EventView', 'event', 'Events')
}

export const sagas = [
  watchForLoadView,
  watchForScrollAction,
  watchForActionRequested
]
