import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import { createRootReducer } from 'nextcloud-react'

export const history = createBrowserHistory()

const sagaMiddleware = createSagaMiddleware()
const composedEnhancer = composeWithDevTools(
  applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware
  )
)

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['app', 'router', 'loading'],
  stateReconciler: autoMergeLevel2
}

export default function createDefaultStore (sagas, customReducer, preloadedState) {
  const persistedReducer = persistReducer(persistConfig, createRootReducer(history, customReducer))
  const store = createStore(
    persistedReducer,
    preloadedState,
    composedEnhancer
  )
  const persistor = persistStore(store)
  sagaMiddleware.run(sagas)
  return { store, persistor }
}
