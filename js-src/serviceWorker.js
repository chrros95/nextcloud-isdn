import { BackgroundSyncPlugin } from 'workbox-background-sync'

import { precacheAndRoute } from 'workbox-precaching'
import { CacheableResponsePlugin } from 'workbox-cacheable-response/CacheableResponsePlugin'
import { CacheFirst, NetworkOnly } from 'workbox-strategies'
import { ExpirationPlugin } from 'workbox-expiration/ExpirationPlugin'
import { registerRoute } from 'workbox-routing/registerRoute'

precacheAndRoute(self.__WB_MANIFEST)

registerRoute(/\.(?:png|jpg|jpeg|svg)$/, new CacheFirst({
  cacheName: 'img',
  matchOptions: {
    ignoreVary: true
  },
  plugins: [new ExpirationPlugin({
    maxEntries: 500,
    maxAgeSeconds: 63072e3,
    purgeOnQuotaError: true
  }), new CacheableResponsePlugin({
    statuses: [0, 200]
  })]
}))

const bgSync = new BackgroundSyncPlugin('checkin-queue', {
  maxRetentionTime: 14 * 24 * 60
})

registerRoute(
  /\/api\/v1\/Events\/\d+\/checkin/,
  new NetworkOnly({
    plugins: [bgSync]
  }),
  'POST'
)

addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting()
  }
})
