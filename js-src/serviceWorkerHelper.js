import { generateFilePath } from '@nextcloud/router'
import { config } from 'config'
import { Workbox } from 'workbox-window'

export function register () {
  if ('serviceWorker' in navigator) {
    // Use the window load event to keep the page load performant
    window.addEventListener('load', () => {
      const publicUrl = generateFilePath(config.appID, 'js', 'serviceWorker.js')
      const wb = new Workbox(publicUrl, { scope: '/' })
      wb.register()
    })
  }
}
