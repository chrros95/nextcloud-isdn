const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const UnusedWebpackPlugin = require('unused-webpack-plugin')
const Visualizer = require('webpack-visualizer-plugin2')
const WorkboxPlugin = require('workbox-webpack-plugin')

module.exports = {
  entry: {
    main: path.join(__dirname, 'main', 'index.js'),
    checkin: path.join(__dirname, 'checkin', 'index.js')
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, '/../js'),
    publicPath: '/custom_apps/isdn/js/',
    chunkFilename: '[name].js?v=[contenthash]'
  },
  resolve: {
    symlinks: false,
    modules: [__dirname, 'node_modules']
  },
  module: {
    rules: [
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 10240
        }
      }, {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            plugins: [
              '@babel/plugin-syntax-dynamic-import',
              '@babel/plugin-proposal-class-properties'
            ],
            presets: [
              '@babel/preset-env',
              '@babel/preset-react'
            ]
          }
        }
      }, {
        test: /\.(s[ac]ss|css)$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new Visualizer({
      filename: './statistics.html'
    }),
    new UnusedWebpackPlugin({
      directories: [path.join(__dirname, 'js-src')]
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: path.join(__dirname, 'serviceWorker.js')
    })
  ]
}
